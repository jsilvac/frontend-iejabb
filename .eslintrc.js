module.exports = {
  parser: 'babel-eslint',
  extends: ['react-app', 'plugin:prettier/recommended'],
  env: {
    jest: true
  },
  plugins: ['prettier'],
  rules: {
    quotes: [2, 'single', 'avoid-escape'],
    'array-element-newline': ['error', 'consistent'],
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'comma-dangle': 'off',
    'arrow-parens': 'off'
  },
  globals: {
    fetch: false
  }
};
