import {
  FETCH_EVENT_LIST_START,
  FETCH_EVENT_LIST_SUCCESS,
  FETCH_EVENT_LIST_ERROR,
  CREATE_EVENT_START,
  CREATE_EVENT_SUCCESS,
  CREATE_EVENT_ERROR,
  EDIT_EVENT_START,
  EDIT_EVENT_SUCCESS,
  EDIT_EVENT_ERROR,
  DELETE_EVENT_START,
  DELETE_EVENT_SUCCESS,
  DELETE_EVENT_ERROR,
  RESET_EVENT_SUCCESS,
  RESET_EVENT_ERROR,
  SEARCH_EVENT_START,
  SEARCH_EVENT_SUCCESS,
  SEARCH_EVENT_ERROR
} from '../actions/types';

const initialState = {
  list: [],
  searchResults: [],
  fetch_error: '',
  fetch_isLoading: false,
  fetch_success: false,
  create_error: '',
  create_isLoading: false,
  create_success: false,
  edit_error: '',
  edit_isLoading: false,
  edit_success: false,
  delete_error: '',
  delete_isLoading: false,
  delete_success: false,
  search_error: '',
  search_isLoading: false,
  search_success: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_EVENT_LIST_START:
      return {
        ...state,
        fetch_error: '',
        fetch_isLoading: true,
        fetch_success: false
      };
    case FETCH_EVENT_LIST_SUCCESS:
      return {
        ...state,
        list: action.payload.data.list,
        fetch_error: '',
        fetch_isLoading: false,
        fetch_success: true
      };
    case FETCH_EVENT_LIST_ERROR:
      return {
        ...state,
        fetch_error: action.payload.error,
        fetch_isLoading: false,
        fetch_success: false
      };
    case CREATE_EVENT_START:
      return {
        ...state,
        create_error: '',
        create_isLoading: true,
        create_success: false
      };
    case CREATE_EVENT_SUCCESS:
      return {
        ...state,
        create_error: '',
        create_isLoading: false,
        create_success: true
      };
    case CREATE_EVENT_ERROR:
      return {
        ...state,
        create_error: action.payload.error,
        create_isLoading: false,
        create_success: false
      };
    case EDIT_EVENT_START:
      return {
        ...state,
        edit_error: '',
        edit_isLoading: true,
        edit_success: false
      };
    case EDIT_EVENT_SUCCESS:
      return {
        ...state,
        edit_error: '',
        edit_isLoading: false,
        edit_success: true
      };
    case EDIT_EVENT_ERROR:
      return {
        ...state,
        edit_error: action.payload.error,
        edit_isLoading: false,
        edit_success: false
      };
    case DELETE_EVENT_START:
      return {
        ...state,
        delete_error: '',
        delete_isLoading: true,
        delete_success: false
      };
    case DELETE_EVENT_SUCCESS:
      return {
        ...state,
        delete_error: '',
        delete_isLoading: false,
        delete_success: true
      };
    case DELETE_EVENT_ERROR:
      return {
        ...state,
        delete_error: action.payload.error,
        delete_isLoading: false,
        delete_success: false
      };
    case SEARCH_EVENT_START:
      return {
        ...state,
        search_error: '',
        search_isLoading: true,
        search_success: false
      };
    case SEARCH_EVENT_SUCCESS:
      return {
        ...state,
        searchResults: action.payload.data.list,
        search_error: '',
        search_isLoading: false,
        search_success: true
      };
    case SEARCH_EVENT_ERROR:
      return {
        ...state,
        search_error: action.payload.error,
        search_isLoading: false,
        search_success: false
      };
    case RESET_EVENT_SUCCESS:
      return {
        ...state,
        fetch_success: false,
        create_success: false,
        edit_success: false,
        delete_success: false
      };
    case RESET_EVENT_ERROR:
      return {
        ...state,
        fetch_error: '',
        create_error: '',
        edit_error: '',
        delete_error: ''
      };
    default:
      break;
  }
  return state;
}

// Getters
export const getEventList = state => state.list;
export const getEventSearch = state => state.searchResults;
export const getOneEvent = (state, id) =>
  state.list.find(event => event._id === id);
