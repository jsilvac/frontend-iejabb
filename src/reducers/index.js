import { combineReducers } from 'redux';

import news, * as fromNews from './news';
import event, * as fromEvent from './event';
import schoolInfo, * as fromSchoolInfo from './schoolInfo';
import auth, * as fromAuth from './auth';
import pqr from './pqr';

export default combineReducers({
  event,
  news,
  schoolInfo,
  pqr,
  auth
});

// News Getters
export const getNewsList = state => fromNews.getNewsList(state.news);

export const getNewsSearch = state => fromNews.getNewsSearch(state.news);

export const getOneNews = (state, id) => fromNews.getOneNews(state.news, id);

// Event Getters
export const getEventList = state => fromEvent.getEventList(state.event);

export const getEventSearch = state => fromEvent.getEventSearch(state.event);

export const getOneEvent = (state, id) =>
  fromEvent.getOneEvent(state.event, id);

// SchoolInfo Getters

export const getSchoolPlatformUrl = state =>
  fromSchoolInfo.getPlatformUrl(state.schoolInfo);

export const getContactInfo = state =>
  fromSchoolInfo.getContactInfo(state.schoolInfo);

export const getInstitutionalInfo = state =>
  fromSchoolInfo.getInstitutionalInfo(state.schoolInfo);

export const getAdminCouncilInfo = state =>
  fromSchoolInfo.getAdminCouncilInfo(state.schoolInfo);

export const getAcademicCouncilInfo = state =>
  fromSchoolInfo.getAcademicCouncilInfo(state.schoolInfo);

export const getStudentCouncilInfo = state =>
  fromSchoolInfo.getStudentCouncilInfo(state.schoolInfo);

export const getSchoolLifeCouncilInfo = state =>
  fromSchoolInfo.getSchoolLifeCouncilInfo(state.schoolInfo);

export const getProfessorsInfo = state =>
  fromSchoolInfo.getProfessorsInfo(state.schoolInfo);

export const getBlogsInfo = state =>
  fromSchoolInfo.getBlogsInfo(state.schoolInfo);

export const getRules = state => fromSchoolInfo.getRules(state.schoolInfo);

// Auth

export const isUserAuthenticated = state =>
  fromAuth.isUserAuthenticated(state.auth);

export const getUser = state => fromAuth.getUser(state.auth);
