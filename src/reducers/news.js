import {
  FETCH_NEWS_LIST_START,
  FETCH_NEWS_LIST_SUCCESS,
  FETCH_NEWS_LIST_ERROR,
  CREATE_NEWS_START,
  CREATE_NEWS_SUCCESS,
  CREATE_NEWS_ERROR,
  EDIT_NEWS_START,
  EDIT_NEWS_SUCCESS,
  EDIT_NEWS_ERROR,
  DELETE_NEWS_START,
  DELETE_NEWS_SUCCESS,
  DELETE_NEWS_ERROR,
  RESET_NEWS_SUCCESS,
  RESET_NEWS_ERROR,
  SEARCH_NEWS_START,
  SEARCH_NEWS_SUCCESS,
  SEARCH_NEWS_ERROR
} from '../actions/types';

const initialState = {
  list: [],
  searchResults: [],
  fetch_error: '',
  fetch_isLoading: false,
  fetch_success: false,
  create_error: '',
  create_isLoading: false,
  create_success: false,
  edit_error: '',
  edit_isLoading: false,
  edit_success: false,
  delete_error: '',
  delete_isLoading: false,
  delete_success: false,
  search_error: '',
  search_isLoading: false,
  search_success: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_NEWS_LIST_START:
      return {
        ...state,
        fetch_error: '',
        fetch_isLoading: true,
        fetch_success: false
      };
    case FETCH_NEWS_LIST_SUCCESS:
      return {
        ...state,
        list: action.payload.data.list,
        fetch_error: '',
        fetch_isLoading: false,
        fetch_success: true
      };
    case FETCH_NEWS_LIST_ERROR:
      return {
        ...state,
        fetch_error: action.payload.error,
        fetch_isLoading: false,
        fetch_success: false
      };
    case CREATE_NEWS_START:
      return {
        ...state,
        create_error: '',
        create_isLoading: true,
        create_success: false
      };
    case CREATE_NEWS_SUCCESS:
      return {
        ...state,
        create_error: '',
        create_isLoading: false,
        create_success: true
      };
    case CREATE_NEWS_ERROR:
      return {
        ...state,
        create_error: action.payload.error,
        create_isLoading: false,
        create_success: false
      };
    case EDIT_NEWS_START:
      return {
        ...state,
        edit_error: '',
        edit_isLoading: true,
        edit_success: false
      };
    case EDIT_NEWS_SUCCESS:
      return {
        ...state,
        edit_error: '',
        edit_isLoading: false,
        edit_success: true
      };
    case EDIT_NEWS_ERROR:
      return {
        ...state,
        edit_error: action.payload.error,
        edit_isLoading: false,
        edit_success: false
      };
    case DELETE_NEWS_START:
      return {
        ...state,
        delete_error: '',
        delete_isLoading: true,
        delete_success: false
      };
    case DELETE_NEWS_SUCCESS:
      return {
        ...state,
        delete_error: '',
        delete_isLoading: false,
        delete_success: true
      };
    case DELETE_NEWS_ERROR:
      return {
        ...state,
        delete_error: action.payload.error,
        delete_isLoading: false,
        delete_success: false
      };
    case SEARCH_NEWS_START:
      return {
        ...state,
        search_error: '',
        search_isLoading: true,
        search_success: false
      };
    case SEARCH_NEWS_SUCCESS:
      return {
        ...state,
        searchResults: action.payload.data.list,
        search_error: '',
        search_isLoading: false,
        search_success: true
      };
    case SEARCH_NEWS_ERROR:
      return {
        ...state,
        search_error: action.payload.error,
        search_isLoading: false,
        search_success: false
      };
    case RESET_NEWS_SUCCESS:
      return {
        ...state,
        fetch_success: false,
        create_success: false,
        edit_success: false,
        delete_success: false,
        search_success: false
      };
    case RESET_NEWS_ERROR:
      return {
        ...state,
        fetch_error: '',
        create_error: '',
        edit_error: '',
        delete_error: '',
        search_error: ''
      };
    default:
      break;
  }
  return state;
}

// Getters
export const getNewsList = state => state.list;
export const getNewsSearch = state => state.searchResults;
export const getOneNews = (state, id) =>
  state.list.find(news => news._id === id);
