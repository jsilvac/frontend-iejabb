import {
  SEND_PQR_START,
  SEND_PQR_SUCCESS,
  SEND_PQR_ERROR,
  RESET_PQR_SUCCESS,
  RESET_PQR_ERROR
} from '../actions/types';

const initialState = {
  send_error: '',
  send_isLoading: false,
  send_success: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SEND_PQR_START:
      return {
        ...state,
        send_error: '',
        send_isLoading: true,
        send_success: false
      };
    case SEND_PQR_SUCCESS:
      return {
        ...state,
        send_error: '',
        send_isLoading: false,
        send_success: true
      };
    case SEND_PQR_ERROR:
      return {
        ...state,
        send_error: action.payload.error,
        send_isLoading: false,
        send_success: false
      };
    case RESET_PQR_SUCCESS:
      return {
        ...state,
        send_success: false
      };
    case RESET_PQR_ERROR:
      return {
        ...state,
        send_error: ''
      };
    default:
      break;
  }
  return state;
}
