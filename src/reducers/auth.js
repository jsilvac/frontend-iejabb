import {
  AUTH_SIGNIN_START,
  AUTH_SIGNIN_SUCCESS,
  AUTH_SIGNIN_ERROR,
  AUTH_LOGOUT
} from '../actions/types';

const user = JSON.parse(sessionStorage.getItem('user'));

const initialState = {
  user: user ? user.data : null,
  authenticated: user ? true : false,
  signIn_error: '',
  signIn_isLoading: false,
  signIn_success: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case AUTH_SIGNIN_START:
      return {
        ...state,
        signIn_error: '',
        signIn_isLoading: true,
        signIn_success: false
      };
    case AUTH_SIGNIN_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        authenticated: true,
        signIn_error: '',
        signIn_isLoading: false,
        signIn_success: true
      };
    case AUTH_SIGNIN_ERROR:
      return {
        ...state,
        user: null,
        authenticated: false,
        signIn_error: action.payload.error,
        signIn_isLoading: false,
        signIn_success: false
      };
    case AUTH_LOGOUT:
      return {
        ...state,
        user: null,
        authenticated: false,
        signIn_success: false
      };
    default:
      break;
  }
  return state;
}

export const isUserAuthenticated = state => state.authenticated;
export const getUser = state => state.user;
