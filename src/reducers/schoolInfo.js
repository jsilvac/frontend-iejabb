import {
  FETCH_SCHOOL_INFO_START,
  FETCH_SCHOOL_INFO_SUCCESS,
  FETCH_SCHOOL_INFO_ERROR,
  SET_SCHOOL_INFO_SECTION_ID,
  EDIT_SI_CONTACT_START,
  EDIT_SI_CONTACT_SUCCESS,
  EDIT_SI_CONTACT_ERROR,
  EDIT_SI_INSTITU_INFO_START,
  EDIT_SI_INSTITU_INFO_SUCCESS,
  EDIT_SI_INSTITU_INFO_ERROR,
  EDIT_SI_ADMIN_COUNCIL_START,
  EDIT_SI_ADMIN_COUNCIL_SUCCESS,
  EDIT_SI_ADMIN_COUNCIL_ERROR,
  EDIT_SI_ACADEMIC_COUNCIL_START,
  EDIT_SI_ACADEMIC_COUNCIL_SUCCESS,
  EDIT_SI_ACADEMIC_COUNCIL_ERROR,
  EDIT_SI_SCHOOL_LIFE_COUNCIL_START,
  EDIT_SI_SCHOOL_LIFE_COUNCIL_SUCCESS,
  EDIT_SI_SCHOOL_LIFE_COUNCIL_ERROR,
  EDIT_SI_STUDENT_COUNCIL_START,
  EDIT_SI_STUDENT_COUNCIL_SUCCESS,
  EDIT_SI_STUDENT_COUNCIL_ERROR,
  EDIT_SI_PROFESSORS_START,
  EDIT_SI_PROFESSORS_SUCCESS,
  EDIT_SI_PROFESSORS_ERROR,
  EDIT_SI_BLOGS_START,
  EDIT_SI_BLOGS_SUCCESS,
  EDIT_SI_BLOGS_ERROR,
  EDIT_SI_RULES_START,
  EDIT_SI_RULES_SUCCESS,
  EDIT_SI_RULES_ERROR
} from '../actions/types';
import { infoSectionID } from '../constants/numbers';

const initialState = {
  data: null,
  error: '',
  isLoading: false,
  selectedSectionID: infoSectionID.institutionalInfo,
  contact_isLoading: false,
  contact_error: '',
  contact_success: false,
  instituInfo_isLoading: false,
  instituInfo_error: '',
  instituInfo_success: false,
  adminCouncil_isLoading: false,
  adminCouncil_error: '',
  adminCouncil_success: false,
  academicCouncil_isLoading: false,
  academicCouncil_error: '',
  academicCouncil_success: false,
  schoolLifeCouncil_isLoading: false,
  schoolLifeCouncil_error: '',
  schoolLifeCouncil_success: false,
  studentCouncil_isLoading: false,
  studentCouncil_error: '',
  studentCouncil_success: false,
  professors_isLoading: false,
  professors_error: '',
  professors_success: false,
  blogs_isLoading: false,
  blogs_error: '',
  blogs_success: false,
  rules_isLoading: false,
  rules_error: '',
  rules_success: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_SCHOOL_INFO_SECTION_ID:
      return {
        ...state,
        selectedSectionID: action.payload.sectionID
      };
    case FETCH_SCHOOL_INFO_START:
      return {
        ...state,
        error: '',
        isLoading: true
      };
    case FETCH_SCHOOL_INFO_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        error: '',
        isLoading: false
      };
    case FETCH_SCHOOL_INFO_ERROR:
      return {
        ...state,
        error: action.payload.error,
        isLoading: false
      };
    case EDIT_SI_CONTACT_START:
      return {
        ...state,
        contact_isLoading: true,
        contact_error: '',
        contact_success: false
      };
    case EDIT_SI_CONTACT_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        contact_isLoading: false,
        contact_error: '',
        contact_success: true
      };
    case EDIT_SI_CONTACT_ERROR:
      return {
        ...state,
        contact_isLoading: false,
        contact_error: action.payload.error,
        contact_success: false
      };
    case EDIT_SI_INSTITU_INFO_START:
      return {
        ...state,
        instituInfo_isLoading: true,
        instituInfo_error: '',
        instituInfo_success: false
      };
    case EDIT_SI_INSTITU_INFO_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        instituInfo_isLoading: false,
        instituInfo_error: '',
        instituInfo_success: true
      };
    case EDIT_SI_INSTITU_INFO_ERROR:
      return {
        ...state,
        instituInfo_isLoading: false,
        instituInfo_error: action.payload.error,
        instituInfo_success: false
      };
    case EDIT_SI_ADMIN_COUNCIL_START:
      return {
        ...state,
        adminCouncil_isLoading: true,
        adminCouncil_error: '',
        adminCouncil_success: false
      };
    case EDIT_SI_ADMIN_COUNCIL_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        adminCouncil_isLoading: false,
        adminCouncil_error: '',
        adminCouncil_success: true
      };
    case EDIT_SI_ADMIN_COUNCIL_ERROR:
      return {
        ...state,
        adminCouncil_isLoading: false,
        adminCouncil_error: action.payload.error,
        adminCouncil_success: false
      };
    case EDIT_SI_ACADEMIC_COUNCIL_START:
      return {
        ...state,
        academicCouncil_isLoading: true,
        academicCouncil_error: '',
        academicCouncil_success: false
      };
    case EDIT_SI_ACADEMIC_COUNCIL_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        academicCouncil_isLoading: false,
        academicCouncil_error: '',
        academicCouncil_success: true
      };
    case EDIT_SI_ACADEMIC_COUNCIL_ERROR:
      return {
        ...state,
        academicCouncil_isLoading: false,
        academicCouncil_error: action.payload.error,
        academicCouncil_success: false
      };
    case EDIT_SI_SCHOOL_LIFE_COUNCIL_START:
      return {
        ...state,
        schoolLifeCouncil_isLoading: true,
        schoolLifeCouncil_error: '',
        schoolLifeCouncil_success: false
      };
    case EDIT_SI_SCHOOL_LIFE_COUNCIL_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        schoolLifeCouncil_isLoading: false,
        schoolLifeCouncil_error: '',
        schoolLifeCouncil_success: true
      };
    case EDIT_SI_SCHOOL_LIFE_COUNCIL_ERROR:
      return {
        ...state,
        schoolLifeCouncil_isLoading: false,
        schoolLifeCouncil_error: action.payload.error,
        schoolLifeCouncil_success: false
      };
    case EDIT_SI_STUDENT_COUNCIL_START:
      return {
        ...state,
        studentCouncil_isLoading: true,
        studentCouncil_error: '',
        studentCouncil_success: false
      };
    case EDIT_SI_STUDENT_COUNCIL_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        studentCouncil_isLoading: false,
        studentCouncil_error: '',
        studentCouncil_success: true
      };
    case EDIT_SI_STUDENT_COUNCIL_ERROR:
      return {
        ...state,
        studentCouncil_isLoading: false,
        studentCouncil_error: action.payload.error,
        studentCouncil_success: false
      };
    case EDIT_SI_PROFESSORS_START:
      return {
        ...state,
        professors_isLoading: true,
        professors_error: '',
        professors_success: false
      };
    case EDIT_SI_PROFESSORS_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        professors_isLoading: false,
        professors_error: '',
        professors_success: true
      };
    case EDIT_SI_PROFESSORS_ERROR:
      return {
        ...state,
        professors_isLoading: false,
        professors_error: action.payload.error,
        professors_success: false
      };
    case EDIT_SI_BLOGS_START:
      return {
        ...state,
        blogs_isLoading: true,
        blogs_error: '',
        blogs_success: false
      };
    case EDIT_SI_BLOGS_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        blogs_isLoading: false,
        blogs_error: '',
        blogs_success: true
      };
    case EDIT_SI_BLOGS_ERROR:
      return {
        ...state,
        blogs_isLoading: false,
        blogs_error: action.payload.error,
        blogs_success: false
      };
    case EDIT_SI_RULES_START:
      return {
        ...state,
        rules_isLoading: true,
        rules_error: '',
        rules_success: false
      };
    case EDIT_SI_RULES_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
        rules_isLoading: false,
        rules_error: '',
        rules_success: true
      };
    case EDIT_SI_RULES_ERROR:
      return {
        ...state,
        rules_isLoading: false,
        rules_error: action.payload.error,
        rules_success: false
      };
    default:
      break;
  }
  return state;
}

// Getters
export const getPlatformUrl = state =>
  !state.data ? '' : state.data.contact.platform;
export const getContactInfo = state =>
  !state.data ? null : state.data.contact;
export const getInstitutionalInfo = state =>
  !state.data ? null : state.data.institutionalInfo;
export const getAdminCouncilInfo = state =>
  !state.data ? null : state.data.institutionalGoverment.adminCouncil;
export const getAcademicCouncilInfo = state =>
  !state.data ? null : state.data.institutionalGoverment.academicCouncil;
export const getStudentCouncilInfo = state =>
  !state.data ? null : state.data.institutionalGoverment.studentCouncil;
export const getSchoolLifeCouncilInfo = state =>
  !state.data ? null : state.data.institutionalGoverment.schoolLifeCouncil;
export const getProfessorsInfo = state =>
  !state.data ? null : state.data.professors;
export const getBlogsInfo = state => (!state.data ? '' : state.data.blogs);
export const getRules = state => (!state.data ? '' : state.data.rules);
