import { takeEvery, call, put } from 'redux-saga/effects';
import { createNews } from './../server/api';
import {
  FETCH_NEWS_LIST_START,
  CREATE_NEWS_START,
  CREATE_NEWS_SUCCESS,
  CREATE_NEWS_ERROR
} from './../actions/types';

function* createNewsCall(action) {
  const response = yield call(createNews, action.payload);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: CREATE_NEWS_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: CREATE_NEWS_SUCCESS,
      payload: { data: dataParsed }
    });
    yield put({
      type: FETCH_NEWS_LIST_START
    });
  }
}

export default function* watchCreateNewsCall() {
  yield takeEvery(CREATE_NEWS_START, createNewsCall);
}
