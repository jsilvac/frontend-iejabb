import { takeEvery, call, put } from 'redux-saga/effects';
import { searchEvent } from '../server/api';
import {
  SEARCH_EVENT_START,
  SEARCH_EVENT_SUCCESS,
  SEARCH_EVENT_ERROR
} from '../actions/types';

function* searchEventCall(action) {
  const { content } = action.payload;
  const response = yield call(searchEvent, content);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: SEARCH_EVENT_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: SEARCH_EVENT_SUCCESS,
      payload: { data: dataParsed }
    });
  }
}

export default function* watchSearchEventCall() {
  yield takeEvery(SEARCH_EVENT_START, searchEventCall);
}
