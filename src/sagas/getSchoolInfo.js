import { takeEvery, call, put } from 'redux-saga/effects';
import { fetchSchoolInfo } from '../server/api';
import {
  FETCH_SCHOOL_INFO_START,
  FETCH_SCHOOL_INFO_SUCCESS,
  FETCH_SCHOOL_INFO_ERROR
} from '../actions/types';

function* getSchoolInfo() {
  const response = yield call(fetchSchoolInfo);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: FETCH_SCHOOL_INFO_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: FETCH_SCHOOL_INFO_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchGetSchoolInfo() {
  yield takeEvery(FETCH_SCHOOL_INFO_START, getSchoolInfo);
}
