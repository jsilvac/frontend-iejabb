import { takeEvery, call, put } from 'redux-saga/effects';
import { searchNews } from '../server/api';
import {
  SEARCH_NEWS_START,
  SEARCH_NEWS_SUCCESS,
  SEARCH_NEWS_ERROR
} from '../actions/types';

function* searchNewsCall(action) {
  const { content } = action.payload;
  const response = yield call(searchNews, content);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: SEARCH_NEWS_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: SEARCH_NEWS_SUCCESS,
      payload: { data: dataParsed }
    });
  }
}

export default function* watchSearchNewsCall() {
  yield takeEvery(SEARCH_NEWS_START, searchNewsCall);
}
