import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIAcademicCouncil } from '../server/api';
import {
  EDIT_SI_ACADEMIC_COUNCIL_START,
  EDIT_SI_ACADEMIC_COUNCIL_SUCCESS,
  EDIT_SI_ACADEMIC_COUNCIL_ERROR
} from '../actions/types';

function* editSIAcademicCouncilCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIAcademicCouncil, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_ACADEMIC_COUNCIL_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_ACADEMIC_COUNCIL_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIAcademicCouncilCall() {
  yield takeEvery(EDIT_SI_ACADEMIC_COUNCIL_START, editSIAcademicCouncilCall);
}
