import { takeEvery, call, put } from 'redux-saga/effects';
import { login } from '../server/api';
import {
  AUTH_SIGNIN_START,
  AUTH_SIGNIN_SUCCESS,
  AUTH_SIGNIN_ERROR
} from '../actions/types';

function* signInCall(action) {
  const response = yield call(login, action.payload);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: AUTH_SIGNIN_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    sessionStorage.setItem('user', JSON.stringify(response.data.user));
    sessionStorage.setItem('token', JSON.stringify(response.data.token));
    yield put({
      type: AUTH_SIGNIN_SUCCESS,
      payload: { user: response.data.user }
    });
  }
}

export default function* watchSignInCall() {
  yield takeEvery(AUTH_SIGNIN_START, signInCall);
}
