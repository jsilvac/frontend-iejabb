import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIAdminCouncil } from '../server/api';
import {
  EDIT_SI_ADMIN_COUNCIL_START,
  EDIT_SI_ADMIN_COUNCIL_SUCCESS,
  EDIT_SI_ADMIN_COUNCIL_ERROR
} from '../actions/types';

function* editSIAdminCouncilCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIAdminCouncil, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_ADMIN_COUNCIL_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_ADMIN_COUNCIL_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIAdminCouncilCall() {
  yield takeEvery(EDIT_SI_ADMIN_COUNCIL_START, editSIAdminCouncilCall);
}
