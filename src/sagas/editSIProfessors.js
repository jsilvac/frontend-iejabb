import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIProfessors } from '../server/api';
import {
  EDIT_SI_PROFESSORS_START,
  EDIT_SI_PROFESSORS_SUCCESS,
  EDIT_SI_PROFESSORS_ERROR
} from '../actions/types';

function* editSIProfessorsCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIProfessors, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_PROFESSORS_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_PROFESSORS_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIProfessorsCall() {
  yield takeEvery(EDIT_SI_PROFESSORS_START, editSIProfessorsCall);
}
