import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIBlogs } from '../server/api';
import {
  EDIT_SI_BLOGS_START,
  EDIT_SI_BLOGS_SUCCESS,
  EDIT_SI_BLOGS_ERROR
} from '../actions/types';

function* editSIBlogsCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIBlogs, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_BLOGS_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_BLOGS_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIBlogsCall() {
  yield takeEvery(EDIT_SI_BLOGS_START, editSIBlogsCall);
}
