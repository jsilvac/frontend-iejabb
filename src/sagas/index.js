import { all } from 'redux-saga/effects';
import watchGetNewsList from './getNewsList';
import watchCreateNewsCall from './createNews';
import watchRemoveNewsCall from './deleteNews';
import watchEditNewsCall from './editNews';
import watchGetEventsList from './getEventsList';
import watchCreateEventCall from './createEvent';
import watchRemoveEventCall from './deleteEvent';
import watchEditEventCall from './editEvent';
import watchGetSchoolInfo from './getSchoolInfo';
import watchSearchNewsCall from './searchNews';
import watchSearchEventCall from './searchEvent';
import watchSendPQRCall from './sendPQR';
import watchEditSIContactCall from './editSIContact';
import watchEditSIInstituInfoCall from './editSIInstituInfo';
import watchEditSIAdminCouncil from './editSIAdminCouncil';
import watchEditSIAcademicCouncil from './editSIAcademicCouncil';
import watchEditSISchoolLifeCouncil from './editSISchoolLifeCouncil';
import watchEditSIStudentCouncil from './editSIStudentCouncil';
import watchEditSIProfessors from './editSIProfessors';
import watchEditSIBlogs from './editSIBlogs';
import watchEditSIRules from './editSIRules';
import watchSignIn from './signIn';

export default function* rootSaga() {
  yield all([
    watchGetNewsList(),
    watchCreateNewsCall(),
    watchRemoveNewsCall(),
    watchEditNewsCall(),
    watchGetEventsList(),
    watchCreateEventCall(),
    watchRemoveEventCall(),
    watchEditEventCall(),
    watchGetSchoolInfo(),
    watchSearchNewsCall(),
    watchSearchEventCall(),
    watchSendPQRCall(),
    watchEditSIContactCall(),
    watchEditSIInstituInfoCall(),
    watchEditSIAdminCouncil(),
    watchEditSIAcademicCouncil(),
    watchEditSISchoolLifeCouncil(),
    watchEditSIStudentCouncil(),
    watchEditSIProfessors(),
    watchEditSIBlogs(),
    watchEditSIRules(),
    watchSignIn()
  ]);
}
