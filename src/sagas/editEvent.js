import { takeEvery, call, put } from 'redux-saga/effects';
import { editEvent } from '../server/api';
import {
  FETCH_EVENT_LIST_START,
  EDIT_EVENT_START,
  EDIT_EVENT_SUCCESS,
  EDIT_EVENT_ERROR
} from '../actions/types';

function* editEventCall(action) {
  const { id, content } = action.payload;
  const response = yield call(editEvent, id, content);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_EVENT_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: EDIT_EVENT_SUCCESS,
      payload: { data: dataParsed }
    });
    yield put({
      type: FETCH_EVENT_LIST_START
    });
  }
}

export default function* watchEditEventCall() {
  yield takeEvery(EDIT_EVENT_START, editEventCall);
}
