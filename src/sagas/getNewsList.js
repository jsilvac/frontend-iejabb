import { takeEvery, call, put } from 'redux-saga/effects';
import { fetchNewsList } from '../server/api';
import {
  FETCH_NEWS_LIST_START,
  FETCH_NEWS_LIST_SUCCESS,
  FETCH_NEWS_LIST_ERROR
} from '../actions/types';

function* getNewsList() {
  const response = yield call(fetchNewsList);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: FETCH_NEWS_LIST_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: FETCH_NEWS_LIST_SUCCESS,
      payload: { data: dataParsed }
    });
  }
}

export default function* watchGetNewsList() {
  yield takeEvery(FETCH_NEWS_LIST_START, getNewsList);
}
