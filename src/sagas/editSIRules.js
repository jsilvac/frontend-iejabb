import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIRules } from '../server/api';
import {
  EDIT_SI_RULES_START,
  EDIT_SI_RULES_SUCCESS,
  EDIT_SI_RULES_ERROR
} from '../actions/types';

function* editSIRulesCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIRules, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_RULES_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_RULES_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIRulesCall() {
  yield takeEvery(EDIT_SI_RULES_START, editSIRulesCall);
}
