import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIStudentCouncil } from '../server/api';
import {
  EDIT_SI_STUDENT_COUNCIL_START,
  EDIT_SI_STUDENT_COUNCIL_SUCCESS,
  EDIT_SI_STUDENT_COUNCIL_ERROR
} from '../actions/types';

function* editSIStudentCouncilCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIStudentCouncil, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_STUDENT_COUNCIL_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_STUDENT_COUNCIL_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIStudentCouncilCall() {
  yield takeEvery(EDIT_SI_STUDENT_COUNCIL_START, editSIStudentCouncilCall);
}
