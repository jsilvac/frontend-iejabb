import { takeEvery, call, put } from 'redux-saga/effects';
import { editNews } from '../server/api';
import {
  FETCH_NEWS_LIST_START,
  EDIT_NEWS_START,
  EDIT_NEWS_SUCCESS,
  EDIT_NEWS_ERROR
} from '../actions/types';

function* editNewsCall(action) {
  const { id, content } = action.payload;
  const response = yield call(editNews, id, content);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_NEWS_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: EDIT_NEWS_SUCCESS,
      payload: { data: dataParsed }
    });
    yield put({
      type: FETCH_NEWS_LIST_START
    });
  }
}

export default function* watchEditNewsCall() {
  yield takeEvery(EDIT_NEWS_START, editNewsCall);
}
