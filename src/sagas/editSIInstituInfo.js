import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIInstituInfo } from '../server/api';
import {
  EDIT_SI_INSTITU_INFO_START,
  EDIT_SI_INSTITU_INFO_SUCCESS,
  EDIT_SI_INSTITU_INFO_ERROR
} from '../actions/types';

function* editSIInstituInfoCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIInstituInfo, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_INSTITU_INFO_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_INSTITU_INFO_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIInstituInfoCall() {
  yield takeEvery(EDIT_SI_INSTITU_INFO_START, editSIInstituInfoCall);
}
