import { takeEvery, call, put } from 'redux-saga/effects';
import { editSIContact } from '../server/api';
import {
  EDIT_SI_CONTACT_START,
  EDIT_SI_CONTACT_SUCCESS,
  EDIT_SI_CONTACT_ERROR
} from '../actions/types';

function* editSIContactCall(action) {
  const { values } = action.payload;
  const response = yield call(editSIContact, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: EDIT_SI_CONTACT_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: EDIT_SI_CONTACT_SUCCESS,
      payload: { data: response.data }
    });
  }
}

export default function* watchEditSIContactCall() {
  yield takeEvery(EDIT_SI_CONTACT_START, editSIContactCall);
}
