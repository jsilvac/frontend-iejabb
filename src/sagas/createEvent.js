import { takeEvery, call, put } from 'redux-saga/effects';
import { createEvent } from '../server/api';
import {
  FETCH_EVENT_LIST_START,
  CREATE_EVENT_START,
  CREATE_EVENT_SUCCESS,
  CREATE_EVENT_ERROR
} from '../actions/types';

function* createEventCall(action) {
  const response = yield call(createEvent, action.payload);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: CREATE_EVENT_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: CREATE_EVENT_SUCCESS,
      payload: { data: dataParsed }
    });
    yield put({
      type: FETCH_EVENT_LIST_START
    });
  }
}

export default function* watchCreateEventCall() {
  yield takeEvery(CREATE_EVENT_START, createEventCall);
}
