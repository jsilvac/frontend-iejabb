import { takeEvery, call, put } from 'redux-saga/effects';
import { fetchEventsList } from '../server/api';
import {
  FETCH_EVENT_LIST_START,
  FETCH_EVENT_LIST_SUCCESS,
  FETCH_EVENT_LIST_ERROR
} from '../actions/types';

function* getEventsList() {
  const response = yield call(fetchEventsList);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: FETCH_EVENT_LIST_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    const dataParsed = {
      list: response.data
    };
    yield put({
      type: FETCH_EVENT_LIST_SUCCESS,
      payload: { data: dataParsed }
    });
  }
}

export default function* watchGetEventsList() {
  yield takeEvery(FETCH_EVENT_LIST_START, getEventsList);
}
