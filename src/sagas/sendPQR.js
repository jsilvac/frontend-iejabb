import { takeEvery, call, put } from 'redux-saga/effects';
import { sendPQR } from '../server/api';
import {
  SEND_PQR_START,
  SEND_PQR_SUCCESS,
  SEND_PQR_ERROR
} from '../actions/types';

function* sendPQRCall(action) {
  const { values } = action.payload;
  const response = yield call(sendPQR, values);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: SEND_PQR_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: SEND_PQR_SUCCESS
    });
  }
}

export default function* watchSendPQRCall() {
  yield takeEvery(SEND_PQR_START, sendPQRCall);
}
