import { takeEvery, call, put } from 'redux-saga/effects';
import { removeNews } from './../server/api';
import {
  FETCH_NEWS_LIST_START,
  DELETE_NEWS_START,
  DELETE_NEWS_SUCCESS,
  DELETE_NEWS_ERROR
} from './../actions/types';

function* removeNewsCall(action) {
  const response = yield call(removeNews, action.payload);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: DELETE_NEWS_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: DELETE_NEWS_SUCCESS
    });
    yield put({
      type: FETCH_NEWS_LIST_START
    });
  }
}

export default function* watchRemoveNewsCall() {
  yield takeEvery(DELETE_NEWS_START, removeNewsCall);
}
