import { takeEvery, call, put } from 'redux-saga/effects';
import { removeEvent } from '../server/api';
import {
  FETCH_EVENT_LIST_START,
  DELETE_EVENT_START,
  DELETE_EVENT_SUCCESS,
  DELETE_EVENT_ERROR
} from '../actions/types';

function* removeEventCall(action) {
  const response = yield call(removeEvent, action.payload);
  if (Object.prototype.hasOwnProperty.call(response, 'error')) {
    yield put({
      type: DELETE_EVENT_ERROR,
      payload: { error: response.error }
    });
  } else if (response.status >= 200 && response.status < 299) {
    yield put({
      type: DELETE_EVENT_SUCCESS
    });
    yield put({
      type: FETCH_EVENT_LIST_START
    });
  }
}

export default function* watchRemoveEventCall() {
  yield takeEvery(DELETE_EVENT_START, removeEventCall);
}
