import {
  FETCH_EVENT_LIST_START,
  CREATE_EVENT_START,
  EDIT_EVENT_START,
  DELETE_EVENT_START,
  RESET_EVENT_SUCCESS,
  RESET_EVENT_ERROR,
  SEARCH_EVENT_START
} from './types';

export function fetchEvent() {
  return {
    type: FETCH_EVENT_LIST_START
  };
}

export function createEvent(values) {
  return {
    type: CREATE_EVENT_START,
    payload: values
  };
}

export function editEvent(id, content) {
  return {
    type: EDIT_EVENT_START,
    payload: {
      id,
      content
    }
  };
}

export function deleteEvent(id) {
  return {
    type: DELETE_EVENT_START,
    payload: id
  };
}

export function searchEvent(content) {
  return {
    type: SEARCH_EVENT_START,
    payload: { content }
  };
}

export function resetEventSuccess() {
  return {
    type: RESET_EVENT_SUCCESS
  };
}

export function resetEventError() {
  return {
    type: RESET_EVENT_ERROR
  };
}
