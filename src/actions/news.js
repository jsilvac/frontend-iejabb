import {
  FETCH_NEWS_LIST_START,
  CREATE_NEWS_START,
  EDIT_NEWS_START,
  DELETE_NEWS_START,
  RESET_NEWS_SUCCESS,
  RESET_NEWS_ERROR,
  SEARCH_NEWS_START
} from './types';

export function fetchNews() {
  return {
    type: FETCH_NEWS_LIST_START
  };
}

export function createNews(values) {
  return {
    type: CREATE_NEWS_START,
    payload: values
  };
}

export function editNews(id, content) {
  return {
    type: EDIT_NEWS_START,
    payload: {
      id,
      content
    }
  };
}

export function deleteNews(id) {
  return {
    type: DELETE_NEWS_START,
    payload: id
  };
}

export function searchNews(content) {
  return {
    type: SEARCH_NEWS_START,
    payload: { content }
  };
}

export function resetNewsSuccess() {
  return {
    type: RESET_NEWS_SUCCESS
  };
}

export function resetNewsError() {
  return {
    type: RESET_NEWS_ERROR
  };
}
