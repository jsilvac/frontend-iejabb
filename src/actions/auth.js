import { AUTH_SIGNIN_START, AUTH_LOGOUT } from './types';

export function signIn(values) {
  return {
    type: AUTH_SIGNIN_START,
    payload: values
  };
}

export function logOut(values) {
  sessionStorage.removeItem('user');
  sessionStorage.removeItem('token');
  return {
    type: AUTH_LOGOUT
  };
}
