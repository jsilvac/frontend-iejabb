import {
  FETCH_SCHOOL_INFO_START,
  SET_SCHOOL_INFO_SECTION_ID,
  EDIT_SI_CONTACT_START,
  EDIT_SI_ACADEMIC_COUNCIL_START,
  EDIT_SI_ADMIN_COUNCIL_START,
  EDIT_SI_INSTITU_INFO_START,
  EDIT_SI_RULES_START,
  EDIT_SI_SCHOOL_LIFE_COUNCIL_START,
  EDIT_SI_STUDENT_COUNCIL_START,
  EDIT_SI_BLOGS_START,
  EDIT_SI_PROFESSORS_START
} from './types';

export function fetchSchoolInfo() {
  return {
    type: FETCH_SCHOOL_INFO_START
  };
}

export function setSchoolInfoSection(sectionID) {
  return {
    type: SET_SCHOOL_INFO_SECTION_ID,
    payload: { sectionID }
  };
}

export function editSIContact(values) {
  return {
    type: EDIT_SI_CONTACT_START,
    payload: { values }
  };
}

export function editSIInstituInfo(values) {
  return {
    type: EDIT_SI_INSTITU_INFO_START,
    payload: { values }
  };
}

export function editSIAdminCouncil(values) {
  return {
    type: EDIT_SI_ADMIN_COUNCIL_START,
    payload: { values }
  };
}

export function editSIAcademicCouncil(values) {
  return {
    type: EDIT_SI_ACADEMIC_COUNCIL_START,
    payload: { values }
  };
}

export function editSISchoolLifeCouncil(values) {
  return {
    type: EDIT_SI_SCHOOL_LIFE_COUNCIL_START,
    payload: { values }
  };
}

export function editSIStudentCouncil(values) {
  return {
    type: EDIT_SI_STUDENT_COUNCIL_START,
    payload: { values }
  };
}

export function editSIProfessors(values) {
  return {
    type: EDIT_SI_PROFESSORS_START,
    payload: { values }
  };
}

export function editSIBlogs(values) {
  return {
    type: EDIT_SI_BLOGS_START,
    payload: { values }
  };
}

export function editSIRules(values) {
  return {
    type: EDIT_SI_RULES_START,
    payload: { values }
  };
}
