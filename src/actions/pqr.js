import { SEND_PQR_START, RESET_PQR_SUCCESS, RESET_EVENT_ERROR } from './types';

export function sendPQR(values) {
  return {
    type: SEND_PQR_START,
    payload: { values }
  };
}

export function resetPQRSuccess() {
  return {
    type: RESET_PQR_SUCCESS
  };
}

export function resetPQRError() {
  return {
    type: RESET_EVENT_ERROR
  };
}
