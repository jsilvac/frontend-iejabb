import React, { useState } from 'react';
import { connect } from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory, useLocation, Redirect } from 'react-router-dom';

import * as authActions from '../actions/auth';
import * as fromReducers from '../reducers';
import Alert, { alertTypes } from '../components/common/alert';
import CustomBackdrop from '../components/common/customBackdrop';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function SignIn(props) {
  const { signIn, user, success, error, loading } = props;
  const classes = useStyles();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  let history = useHistory();
  let location = useLocation();
  let { from } = location.state || { from: { pathname: '/' } };

  if (success) {
    history.push(from.pathname === '/login' ? '/' : from.pathname);
  }

  const handleSubmitClick = e => {
    e.preventDefault();
    signIn({ email, password });
  };
  if (user) return <Redirect to="/" />;
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Ingresa con tu cuenta
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Correo electrónico"
            name="email"
            autoComplete="email"
            autoFocus
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={e => setPassword(e.target.value)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmitClick}
          >
            Ingresar
          </Button>
        </form>
        <Alert
          open={error !== ''}
          type={alertTypes.error}
          text={'No se ha podido iniciar sesión\n credenciales incorrectas'}
        />
        <CustomBackdrop open={loading} />
      </div>
    </Container>
  );
}

const mapState = state => ({
  authenticated: fromReducers.isUserAuthenticated(state),
  success: state.auth.signIn_success,
  error: state.auth.signIn_error,
  loading: state.auth.signIn_isLoading,
  user: state.auth.user
});

const mapDispatch = dispatch => ({
  signIn: values => dispatch(authActions.signIn(values))
});

export default connect(mapState, mapDispatch)(SignIn);
