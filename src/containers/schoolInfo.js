import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Fab from '@material-ui/core/Fab';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import InfoIcon from '@material-ui/icons/Info';
import GavelIcon from '@material-ui/icons/Gavel';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import HowToRegIcon from '@material-ui/icons/HowToReg';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import StreetviewIcon from '@material-ui/icons/Streetview';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import ListIcon from '@material-ui/icons/List';

import Header from '../components/header';
import Footer from '../components/footer';

import SIContact from '../components/sIContact';
import SIInstituInfo from '../components/sIInstituInfo';
import SIAdminCouncil from '../components/sIAdminCouncil';
import SIAcademicCouncil from '../components/sIAcademicCouncil';
import SIRules from '../components/sIRules';
import SISchoolLifeCouncil from '../components/sISchoolLifeCouncil';
import SIStudentCouncil from '../components/sIStudentCouncil';
import SIProfessors from '../components/sIProfessors';
import SIBlogs from '../components/sIBlogs';
import clsx from 'clsx';

import { infoSectionLabel } from '../constants/strings';
import { infoSectionID } from '../constants/numbers';

import * as schoolInfoActions from '../actions/schoolInfo';

const drawerWidth = 240;

const styles = theme => ({
  container: {
    display: 'flex'
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: '100%'
  },
  root: {
    display: 'flex',
    minHeight: '100vh'
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: 'none'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth,
    position: 'relative'
  },
  fab: {
    position: 'fixed',
    zIndex: 1,
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      padding: 0
    }
  },
  buttonText: {
    padding: '0 8px',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  }
});

const sections = [
  {
    id: infoSectionID.institutionalInfo,
    label: infoSectionLabel.institutionalInfo,
    Icon: InfoIcon
  },
  {
    id: infoSectionID.adminCouncil,
    label: infoSectionLabel.adminCouncil,
    Icon: GavelIcon
  },
  {
    id: infoSectionID.academicCouncil,
    label: infoSectionLabel.academicCouncil,
    Icon: AccountBalanceIcon
  },
  {
    id: infoSectionID.studentCouncil,
    label: infoSectionLabel.studentCouncil,
    Icon: PeopleAltIcon
  },
  {
    id: infoSectionID.schoolLifeCouncil,
    label: infoSectionLabel.schoolLifeCouncil,
    Icon: HowToRegIcon
  },
  {
    id: infoSectionID.professors,
    label: infoSectionLabel.professors,
    Icon: StreetviewIcon
  },
  {
    id: infoSectionID.blogs,
    label: infoSectionLabel.blogs,
    Icon: FormatListBulletedIcon
  },
  {
    id: infoSectionID.rules,
    label: infoSectionLabel.rules,
    Icon: MenuBookIcon
  },
  {
    id: infoSectionID.contact,
    label: infoSectionLabel.contact,
    Icon: ContactSupportIcon
  }
];

const sectionComponent = {
  [infoSectionID.contact]: SIContact,
  [infoSectionID.institutionalInfo]: SIInstituInfo,
  [infoSectionID.adminCouncil]: SIAdminCouncil,
  [infoSectionID.academicCouncil]: SIAcademicCouncil,
  [infoSectionID.studentCouncil]: SIStudentCouncil,
  [infoSectionID.schoolLifeCouncil]: SISchoolLifeCouncil,
  [infoSectionID.professors]: SIProfessors,
  [infoSectionID.blogs]: SIBlogs,
  [infoSectionID.rules]: SIRules
};

const SchoolInfo = props => {
  const { classes, selectedSectionID, setSectionID } = props;
  const SelectedSection = sectionComponent[selectedSectionID];
  const [isDrawerOpen, setDrawerOpen] = useState(false);
  const handleOnClickSection = sectionID => {
    setSectionID(sectionID);
  };

  const toggleDrawer = open => event => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setDrawerOpen(open);
  };

  return (
    <div classes={classes.container}>
      <Header />
      <AppBar
        position="relative"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: isDrawerOpen
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={toggleDrawer(true)}
            edge="start"
            className={clsx(classes.menuButton, isDrawerOpen && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            {sections[selectedSectionID].label}
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.root}>
        <CssBaseline />

        <SwipeableDrawer
          className={classes.drawer}
          open={isDrawerOpen}
          onClose={toggleDrawer(false)}
          onOpen={toggleDrawer(true)}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.toolbar} />
          <List>
            {sections.map(section => (
              <ListItem
                button
                key={section.label}
                onClick={() => handleOnClickSection(section.id)}
              >
                <ListItemIcon>
                  <section.Icon />
                </ListItemIcon>
                <ListItemText primary={section.label} />
              </ListItem>
            ))}
          </List>
          <Divider />
        </SwipeableDrawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <SelectedSection />
        </main>
        <Fab
          variant="extended"
          color="primary"
          aria-label="add"
          className={classes.fab}
          onClick={toggleDrawer(true)}
        >
          <ListIcon />
          <p className={classes.buttonText}>{'Mostrar secciones'}</p>
        </Fab>
      </div>
      <Footer />
    </div>
  );
};

const mapState = state => ({
  selectedSectionID: state.schoolInfo.selectedSectionID
});

const mapDispatch = dispatch => ({
  setSectionID: sectionID =>
    dispatch(schoolInfoActions.setSchoolInfoSection(sectionID))
});

export default connect(mapState, mapDispatch)(withStyles(styles)(SchoolInfo));
