import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from '@material-ui/pickers/';
import Button from '@material-ui/core/Button';
import DateFnsUtils from '@date-io/date-fns';

import NewsContainer from '../components/newsContainer';
import Header from '../components/header';
import Footer from '../components/footer';
import * as fromReducers from '../reducers';

import * as newsActions from '../actions/news';

const styles = theme => ({
  content: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    minHeight: '600px'
  },
  newsContent: {
    flex: 3,
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'space-around'
  },
  newsTitle: {
    padding: 10
  },
  newsList: {
    padding: 10,
    display: 'flex',
    flexDirection: 'row'
  },
  contentContainer: {
    display: 'flex',
    flexDirection: 'column'
  },
  search: {
    display: 'flex',
    width: '100%',
    alignSelf: 'center',
    boxSizing: 'border-box',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: '10px 20px'
  },
  searchItem: {
    margin: '5px',
    minWidth: '250px'
  },
  searchButton: {
    padding: '5px 15px',
    margin: '5px'
  }
});

const NewsSearchPage = props => {
  const {
    classes,
    searching,
    searchResults,
    searchNews,
    newsList,
    fetchNewsList,
    fetching,
    searchSuccess
  } = props;
  const [searchText, setSearchText] = useState('');
  const [initialDate, setInitialDate] = useState(new Date());
  const [finalDate, setFinalDate] = useState(new Date());

  const handleSearchTextChange = event => {
    setSearchText(event.target.value);
  };
  const handleInitialDateChange = date => {
    setInitialDate(date);
  };
  const handleFinalDateChange = date => {
    setFinalDate(date);
  };

  const handleButtonSearchClick = () => {
    searchNews({
      text: searchText,
      initialDate,
      finalDate
    });
  };

  useEffect(() => {
    if (newsList.length === 0 && !searchSuccess) {
      fetchNewsList();
    }
  }, [newsList, fetchNewsList, searchSuccess]);

  return (
    <div className="home">
      <Header />
      <div className={classes.contentContainer}>
        <div className={classes.search}>
          <TextField
            id="standard-basic"
            label="Buscar..."
            value={searchText}
            onChange={handleSearchTextChange}
            className={classes.searchItem}
          />
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              format="dd/MM/yyyy"
              margin="normal"
              id="date-picker-initial"
              label="Fecha inicial"
              value={initialDate}
              onChange={handleInitialDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change initial date'
              }}
              className={classes.searchItem}
            />
            <KeyboardDatePicker
              format="dd/MM/yyyy"
              margin="normal"
              id="date-picker-final"
              label="Fecha final"
              value={finalDate}
              onChange={handleFinalDateChange}
              KeyboardButtonProps={{
                'aria-label': 'change final date'
              }}
              className={classes.searchItem}
            />
          </MuiPickersUtilsProvider>
          <Button
            variant="contained"
            color="primary"
            onClick={handleButtonSearchClick}
            className={classes.searchButton}
          >
            Buscar
          </Button>
        </div>
        <div className={classes.content}>
          <div className={classes.newsContent}>
            <Typography
              className={classes.newsTitle}
              variant="h6"
              color="inherit"
              noWrap
            >
              Resultado de la busqueda
            </Typography>
            <NewsContainer
              newsList={
                searchResults.length !== 0 || searchSuccess
                  ? searchResults
                  : newsList
              }
              loading={searching || fetching}
              stepperOn
              nItemsPStep={6}
            />
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

const mapState = state => ({
  newsList: state.news.list,
  fetching: state.news.fetch_isLoading,
  searching: state.news.search_isLoading,
  searchError: state.news.search_error,
  searchSuccess: state.news.search_success,
  searchResults: fromReducers.getNewsSearch(state)
});

const mapDispatch = dispatch => ({
  searchNews: content => dispatch(newsActions.searchNews(content)),
  fetchNewsList: () => dispatch(newsActions.fetchNews())
});

export default connect(
  mapState,
  mapDispatch
)(withStyles(styles)(NewsSearchPage));
