import React, { Component } from 'react';

import Header from '../components/header';
import SimpleTabs from '../components/common/tabs';
import CRUDNews from '../components/CRUDNews';
import CRUDEvents from '../components/CRUDEvents';
import SIFContact from '../components/sIFContact';
import SIFInstituInfo from '../components/sIFInstituInfo';
import SIFAdminCouncil from '../components/sIFAdminCouncil';
import SIFAcademicCouncil from '../components/sIFAcademicCouncil';
import SIFSchoolLifeCouncil from '../components/sIFSchoolLifeCouncil';
import SIFStudentCouncil from '../components/sIFStudentCouncil';
import SIFProfessors from '../components/sIFProfessors';
import SIFBlogs from '../components/sIFBlogs';
import SIFRules from '../components/sIFRules';

import { infoSectionLabel } from '../constants/strings';

const crudNews = <CRUDNews />;
const crudEvents = <CRUDEvents />;
const sIFContact = <SIFContact />;
const sIFInstituInfo = <SIFInstituInfo />;
const sIFAdminCouncil = <SIFAdminCouncil />;
const sIFAcademicCouncil = <SIFAcademicCouncil />;
const sIFSchoolLifeCouncil = <SIFSchoolLifeCouncil />;
const sIFStudentCouncil = <SIFStudentCouncil />;
const sIFProfessors = <SIFProfessors />;
const sIFBlogs = <SIFBlogs />;
const sIFRules = <SIFRules />;

class Admin extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <div className="admin">
        <Header />
        <SimpleTabs
          views={[
            { name: 'Noticias', component: crudNews },
            { name: 'Eventos', component: crudEvents },
            { name: infoSectionLabel.contact, component: sIFContact },
            {
              name: infoSectionLabel.institutionalInfo,
              component: sIFInstituInfo
            },
            { name: infoSectionLabel.adminCouncil, component: sIFAdminCouncil },
            {
              name: infoSectionLabel.academicCouncil,
              component: sIFAcademicCouncil
            },
            {
              name: infoSectionLabel.schoolLifeCouncil,
              component: sIFSchoolLifeCouncil
            },
            {
              name: infoSectionLabel.studentCouncil,
              component: sIFStudentCouncil
            },
            { name: infoSectionLabel.professors, component: sIFProfessors },
            { name: infoSectionLabel.blogs, component: sIFBlogs },
            { name: infoSectionLabel.rules, component: sIFRules }
          ]}
        />
      </div>
    );
  }
}

export default Admin;
