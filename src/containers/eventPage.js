import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { useParams } from 'react-router-dom';

import * as eventActions from '../actions/event';
import * as fromReducers from '../reducers';

import Header from '../components/header';
import Footer from '../components/footer';
import { formatDate } from '../utils/date';

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  },
  page: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '600px',
    width: '80%',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    }
  },
  img: {
    height: '400px',
    width: '100%',
    objectFit: 'cover'
  },
  title: {
    padding: '10px 20px 0px 20px'
  },
  date: {
    fontWeight: 'bold',
    paddingBottom: '20px',
    paddingLeft: '20px'
  },
  content: {
    padding: '0 20px 10px 20px',
    whiteSpace: 'pre-line'
  }
});

function EventPage(props) {
  const { getEventInfo, fetchEventList, isLoading, classes } = props;
  const { id } = useParams();
  const [eventInfo, setEventInfo] = useState(null);
  useEffect(() => {
    const info = getEventInfo(id);
    if (isLoading) return;
    if (!info) fetchEventList();
    else setEventInfo(info);
  }, [getEventInfo, id, isLoading, fetchEventList]);
  return (
    <div className={classes.container}>
      <Header />
      {eventInfo ? (
        <Paper className={classes.page}>
          <img
            src={eventInfo.img}
            alt={eventInfo.title}
            className={classes.img}
          />
          <Typography variant="h5" component="h3" className={classes.title}>
            {eventInfo.title}
          </Typography>
          <Typography variant="body1" className={classes.date}>
            {formatDate(eventInfo.publishedDate)}
          </Typography>
          <Typography component="p" className={classes.content}>
            {eventInfo.content}
          </Typography>
          <Footer />
        </Paper>
      ) : null}
    </div>
  );
}

const mapState = state => ({
  getEventInfo: id => fromReducers.getOneEvent(state, id),
  isLoading: state.event.fetch_isLoading
});

const mapDispatch = dispatch => ({
  fetchEventList: () => dispatch(eventActions.fetchEvent())
});

export default connect(mapState, mapDispatch)(withStyles(styles)(EventPage));
