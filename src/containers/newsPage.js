import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { useParams } from 'react-router-dom';

import * as newsActions from '../actions/news';
import * as fromReducers from '../reducers';

import Header from '../components/header';
import Footer from '../components/footer';
import { formatDate } from '../utils/date';

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  },
  page: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '600px',
    width: '80%',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    }
  },
  img: {
    height: '400px',
    width: '100%',
    objectFit: 'cover'
  },
  title: {
    padding: '10px 20px 10px 20px'
  },
  author: {
    fontStyle: 'italic',
    textAlign: 'end',
    paddingRight: '20px'
  },
  date: {
    fontWeight: 'bold',
    textAlign: 'end',
    paddingRight: '20px',
    paddingBottom: '20px'
  },
  content: {
    padding: '0 20px 10px 20px',
    whiteSpace: 'pre-line'
  }
});

function NewsPage(props) {
  const { getNewsInfo, fetchNewsList, isLoading, classes } = props;
  const { id } = useParams();
  const [newsInfo, setNewsInfo] = useState(null);
  useEffect(() => {
    const info = getNewsInfo(id);
    if (isLoading) return;
    if (!info) fetchNewsList();
    else setNewsInfo(info);
  }, [getNewsInfo, id, isLoading, fetchNewsList]);
  return (
    <div className={classes.container}>
      <Header />
      {newsInfo ? (
        <Paper className={classes.page}>
          <img
            src={newsInfo.img}
            alt={newsInfo.title}
            className={classes.img}
          />
          <Typography variant="h5" className={classes.title}>
            {newsInfo.title}
          </Typography>
          <Typography variant="body1" className={classes.author}>
            {newsInfo.author}
          </Typography>
          <Typography variant="body1" className={classes.date}>
            {formatDate(newsInfo.publishedDate)}
          </Typography>
          <Typography component="p" className={classes.content}>
            {newsInfo.content}
          </Typography>
          <Footer />
        </Paper>
      ) : null}
    </div>
  );
}

const mapState = state => ({
  getNewsInfo: id => fromReducers.getOneNews(state, id),
  isLoading: state.news.fetch_isLoading
});

const mapDispatch = dispatch => ({
  fetchNewsList: () => dispatch(newsActions.fetchNews())
});

export default connect(mapState, mapDispatch)(withStyles(styles)(NewsPage));
