import React from 'react';
import { connect } from 'react-redux';

import { Route, Switch, Redirect } from 'react-router-dom';
import * as schoolInfoActions from '../actions/schoolInfo';
import PrivateRoute from '../utils/privateRoute';

// Components
import Home from './home';
import Admin from './admin';
import NewsPage from './newsPage';
import NewsSearchPage from './newsSearchPage';
import EventPage from './eventPage';
import EventSearchPage from './eventSearchPage';
import SchoolInfo from './schoolInfo';
import pqrForm from './pqrForm';
import SignIn from './SignIn';

class Router extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount() {
    const { getSchoolInfo } = this.props;
    getSchoolInfo();
  }

  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <PrivateRoute exact path="/admin/">
          <Admin />
        </PrivateRoute>
        <Route exact path="/news/" component={NewsSearchPage} />
        <Route exact path="/news/:id" component={NewsPage} />
        <Route exact path="/event/" component={EventSearchPage} />
        <Route exact path="/event/:id" component={EventPage} />
        <Route exact path="/school-info/" component={SchoolInfo} />
        <Route exact path="/pqrForm/" component={pqrForm} />
        <Route exact path="/login" component={SignIn} />
        <Redirect to="/" />
      </Switch>
    );
  }
}

const mapState = state => ({});

const mapDispatch = dispatch => ({
  getSchoolInfo: () => dispatch(schoolInfoActions.fetchSchoolInfo())
});

export default connect(mapState, mapDispatch)(Router);
