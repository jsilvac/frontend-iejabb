import React, { useState } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import * as pqrActions from '../actions/pqr';
import Header from '../components/header';
import Footer from '../components/footer';
import Alert, { alertTypes } from '../components/common/alert';
const styles = theme => ({
  formContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  form: {
    minWidth: '280px',
    width: '40%',
    display: 'flex',
    flexDirection: 'column',
    margin: '40px'
  },
  textInput: {
    margin: theme.spacing(1)
  },
  button: {
    maxWidth: '80px',
    alignSelf: 'center',
    margin: '10px'
  }
});

function PQRForm(props) {
  const { classes, sendPQR, sendSuccess, sendError } = props;
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [request, setRequest] = useState('');

  const handleChangeName = event => {
    setName(event.target.value);
  };

  const handleChangeEmail = event => {
    setEmail(event.target.value);
  };

  const handleChangeRequest = event => {
    setRequest(event.target.value);
  };

  const handleSubmitButtonClick = () => {
    sendPQR({ name, email, request });
  };

  return (
    <div>
      <Header />
      <div className={classes.formContainer}>
        <div className={classes.form}>
          <TextField
            label="Nombre Completo"
            value={name}
            onChange={handleChangeName}
            className={classes.textInput}
          />
          <TextField
            name="email_user"
            label="Correo electrónico"
            value={email}
            onChange={handleChangeEmail}
            className={classes.textInput}
          />
          <TextField
            name="question"
            label="Dejános tu inquietud..."
            multiline
            rows="4"
            value={request}
            onChange={handleChangeRequest}
            className={classes.textInput}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmitButtonClick}
            className={classes.button}
          >
            Enviar
          </Button>
        </div>
        <Alert
          open={!!(sendSuccess || sendError !== '')}
          type={sendSuccess ? alertTypes.success : alertTypes.error}
          text={
            sendSuccess
              ? 'Se ha enviado la petición'
              : 'Hubo un error al enviar la petición'
          }
        />
      </div>
      <Footer />
    </div>
  );
}

const mapState = state => ({
  sending: state.pqr.send_isLoading,
  sendError: state.pqr.send_error,
  sendSuccess: state.pqr.send_success
});

const mapDispatch = dispatch => ({
  sendPQR: values => dispatch(pqrActions.sendPQR(values))
});

export default withStyles(styles, { withTheme: true })(
  connect(mapState, mapDispatch)(PQRForm)
);
