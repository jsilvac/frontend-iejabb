import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { TwitterTimelineEmbed } from 'react-twitter-embed';

import NewsContainer from '../components/newsContainer';
import Header from '../components/header';
import ImageSlider from '../components/imageSlider';
import Footer from '../components/footer';

import * as fromReducers from '../reducers';

import * as newsActions from '../actions/news';
import * as eventActions from '../actions/event';

const styles = theme => ({
  content: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap'
  },
  newsContent: {
    flex: 3,
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  newsTitle: {
    padding: 10
  },
  newsList: {
    padding: 10,
    display: 'flex',
    flexDirection: 'row'
  },
  twitterContainer: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    boxSizing: 'border-box',
    padding: 20
  }
});

class Home extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount() {
    const { fetchNewsList, fetchEventList } = this.props;
    fetchNewsList();
    fetchEventList();
  }

  render() {
    const { classes, newsList, eventList } = this.props;
    return (
      <div className="home">
        <Header />
        <div>
          <ImageSlider eventList={eventList} />
          <div className={classes.content}>
            <div className={classes.newsContent}>
              <Typography
                className={classes.newsTitle}
                variant="h6"
                color="inherit"
                noWrap
              >
                Noticias Recientes
              </Typography>
              <NewsContainer newsList={newsList} nItemsPStep={6} />
            </div>
            <div className={classes.twitterContainer}>
              <TwitterTimelineEmbed
                sourceType="profile"
                screenName="Mineducacion"
                options={{ height: 900 }}
              />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

const mapState = state => ({
  newsList: fromReducers.getNewsList(state),
  eventList: fromReducers.getEventList(state)
});

const mapDispatch = dispatch => ({
  fetchNewsList: () => dispatch(newsActions.fetchNews()),
  fetchEventList: () => dispatch(eventActions.fetchEvent())
});

export default connect(mapState, mapDispatch)(withStyles(styles)(Home));
