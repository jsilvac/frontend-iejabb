import axios from 'axios';

export const jsonAxiosAPI = axios.create({
  baseURL: `${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}`,
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json'
  }
});

export const multipartAxiosAPI = axios.create({
  baseURL: `${process.env.REACT_APP_BACKEND_URL}:${process.env.REACT_APP_BACKEND_PORT}`,
  timeout: 30000,
  headers: {
    'Content-Type': 'multipart/form-data'
  }
});

export function fetchNewsList() {
  return multipartAxiosAPI
    .get('/api/news')
    .catch(err => ({ error: err.toString() }));
}

export function createNews(newsContent) {
  return multipartAxiosAPI
    .post('/api/news/new', newsContent, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function removeNews(id) {
  return multipartAxiosAPI
    .delete(`/api/news/delete/${id}`, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editNews(id, newsContent) {
  return multipartAxiosAPI
    .put(`/api/news/update/${id}`, newsContent, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function searchNews(content) {
  return multipartAxiosAPI
    .get(
      `/api/news/search/?text=${content.text}&initialDate=${content.initialDate}&finalDate=${content.finalDate}`
    )
    .catch(err => ({ error: err.toString() }));
}

export function fetchEventsList() {
  return multipartAxiosAPI
    .get('/api/events')
    .catch(err => ({ error: err.toString() }));
}

export function createEvent(eventContent) {
  return multipartAxiosAPI
    .post('/api/events/new', eventContent, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function removeEvent(id) {
  return multipartAxiosAPI
    .delete(`/api/events/delete/${id}`, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editEvent(id, eventContent) {
  return multipartAxiosAPI
    .put(`/api/events/update/${id}`, eventContent, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function searchEvent(content) {
  return multipartAxiosAPI
    .get(
      `/api/events/search/?text=${content.text}&initialDate=${content.initialDate}&finalDate=${content.finalDate}`
    )
    .catch(err => ({ error: err.toString() }));
}

export function fetchSchoolInfo() {
  return multipartAxiosAPI
    .get('/api/school-info')
    .catch(err => ({ error: err.toString() }));
}

export function editSIContact(values) {
  return jsonAxiosAPI
    .put('/api/contact', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSIInstituInfo(values) {
  return multipartAxiosAPI
    .put('/api/institutional-info', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSIAdminCouncil(values) {
  return multipartAxiosAPI
    .put('/api/admin-council', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSIAcademicCouncil(values) {
  return multipartAxiosAPI
    .put('/api/academic-council', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSISchoolLifeCouncil(values) {
  return multipartAxiosAPI
    .put('/api/school-life-council', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSIStudentCouncil(values) {
  return multipartAxiosAPI
    .put('/api/student-council', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSIProfessors(values) {
  return multipartAxiosAPI
    .put('/api/professors', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSIBlogs(values) {
  return multipartAxiosAPI
    .put('/api/blogs', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function editSIRules(values) {
  return jsonAxiosAPI
    .put('/api/rules', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function sendPQR(values) {
  return jsonAxiosAPI
    .post('/api/pqr', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function login(values) {
  return jsonAxiosAPI
    .post('/api/auth/login', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}

export function register(values) {
  return jsonAxiosAPI
    .post('/api/auth/register', values, {
      headers: {
        authorization: 'Bearer ' + JSON.parse(sessionStorage.getItem('token'))
      }
    })
    .catch(err => ({ error: err.toString() }));
}
