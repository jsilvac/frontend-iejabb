export const infoSectionLabel = {
  institutionalInfo: 'Información de la escuela',
  adminCouncil: 'Consejo Directivo',
  academicCouncil: 'Consejería Académica',
  studentCouncil: 'Consejo Estudiantil',
  schoolLifeCouncil: 'Comité de convivencia',
  professors: 'Profesores',
  blogs: 'Blogs',
  rules: 'Reglamento Estudiantil',
  contact: 'Contacto'
};
