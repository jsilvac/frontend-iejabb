export const infoSectionID = {
  institutionalInfo: 0,
  adminCouncil: 1,
  academicCouncil: 2,
  studentCouncil: 3,
  schoolLifeCouncil: 4,
  professors: 5,
  blogs: 6,
  rules: 7,
  contact: 8
};
