import React from 'react';
import { Provider } from 'react-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { BrowserRouter } from 'react-router-dom';
import './App.css';

// Components
import Router from './containers/router';

import store from './store';

const theme = createMuiTheme({
  palette: {
    primary: { main: '#110133', text: '#FFF' },
    secondary: { main: '#7c4dff', text: '#999' },
    success: { main: '#4F5' },
    error: { main: '#F55' },
    warning: { main: '#FA5' },
    info: { main: '#58F' }
  },
  typography: { useNextVariants: true },
  textShadows: [`1px 1px ${'#7c4dff'}`]
});

export default function App() {
  return (
    <BrowserRouter>
      <MuiThemeProvider theme={theme}>
        <Provider store={store}>
          <Router />
        </Provider>
      </MuiThemeProvider>
    </BrowserRouter>
  );
}
