import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

import * as fromReducers from '../reducers';

function PrivateRoute({ children, authenticated, ...rest }) {
  console.log(authenticated);
  return (
    <Route
      {...rest}
      render={({ location }) =>
        authenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

const mapState = state => ({
  authenticated: fromReducers.isUserAuthenticated(state)
});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(PrivateRoute);
