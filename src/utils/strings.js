export const TINY_NAME_MAX_CHARACTERS = 14;
export const SHORT_NAME_MAX_CHARACTERS = 30;
export const TINY_DESC_MAX_CHARACTERS = 40;
export const SHORT_DESC_MAX_CHARACTERS = 100;
export const MEDIUM_DESC_MAX_CHARACTERS = 200;
export const truncateString = (str, n) => {
  if (str.length > n) {
    return `${str.substring(0, n)}...`;
  }
  return str;
};
