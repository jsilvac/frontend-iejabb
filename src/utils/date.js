export function formatDate(rawdate) {
  const dateObject = new Date(rawdate);
  const monthNames = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
  ];
  const day = dateObject.getDate();
  const monthIndex = dateObject.getMonth();
  const year = dateObject.getFullYear();

  return `${day} de ${monthNames[monthIndex]} | ${year}`;
}

export const formatName = () => {};
