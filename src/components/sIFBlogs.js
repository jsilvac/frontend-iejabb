import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import * as schoolInfoActions from '../actions/schoolInfo';
import * as fromReducers from '../reducers';
import Alert, { alertTypes } from './common/alert';
import Info from './common/Info';
import Wait from './common/Wait';
import MemberCreator from './common/memberCreator';
import MemberCard from './common/memberCard';
import BLOG_IMAGE from '../assets/images/default_blog.jpg';
import CustomBackdrop from '../components/common/customBackdrop';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center'
  },
  title: {
    margin: '40px 10px'
  },
  content: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    padding: '20px',
    boxSizing: 'border-box'
  },
  membersContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  textInput: {
    margin: theme.spacing(1)
  },
  button: {
    width: 'max-content',
    alignSelf: 'center',
    margin: '20px 10px 0px 10px'
  }
});

const memberFields = [
  { name: 'name', label: 'Nombre' },
  { name: 'link', label: 'URL del blog' }
];

function SIFContact(props) {
  const {
    classes,
    blogs,
    isLoading,
    editBlogs,
    editSuccess,
    editError,
    editing
  } = props;
  const [membersList, setMembersList] = useState([]);
  const [createdMembers, setCreatedMembers] = useState([]);
  const [deletedMembers, setDeletedMembers] = useState([]);
  const [listOfImages, setListOfImages] = useState([]);

  useEffect(() => {
    if (blogs) {
      setMembersList(blogs);
    }
  }, [blogs]);

  const handleSubmitClick = () => {
    const data = {
      createdMembers,
      deletedMembers
    };
    const fData = new FormData();
    fData.append('data', JSON.stringify(data));
    listOfImages.forEach((image, i) => {
      if (image) fData.append('images', image, i);
    });
    setCreatedMembers([]);
    setDeletedMembers([]);
    setListOfImages([]);
    editBlogs(fData);
  };

  const handleCreateMember = data => {
    setCreatedMembers([...createdMembers, data.member]);
    if (data.image) setListOfImages([...listOfImages, data.image]);
  };

  const handleDeleteOldMember = id => {
    setDeletedMembers([...deletedMembers, membersList[id].id]);
    setMembersList(membersList.filter((m, i) => i !== id));
  };

  const handleDeleteNewMember = id => {
    setCreatedMembers(createdMembers.filter((m, i) => i !== id));
    setListOfImages(listOfImages.filter((m, i) => i !== id));
  };

  if (!blogs && isLoading) return <Wait text="Se está cargando los Blogs" />;

  if (!blogs) return <Info text="No se ha cargado los Blogs" />;

  return (
    <div className={classes.container}>
      <Typography variant="h4" component="h6" className={classes.title}>
        Editar Blogs
      </Typography>
      <div className={classes.content}>
        <MemberCreator
          onSubmit={handleCreateMember}
          memberFields={memberFields}
          memberType="blogs"
          memberLabel="Blog"
        />
        <div className={classes.membersContainer}>
          {membersList.map((member, i) => (
            <MemberCard
              key={`${member.name}-${i}`}
              id={i}
              name={member.name}
              position={member.link}
              image={member.pictureUrl}
              onDelete={handleDeleteOldMember}
            />
          ))}
          {createdMembers.map((member, i) => (
            <MemberCard
              key={`${member.name}-${i}`}
              id={i}
              name={member.name}
              position={member.link}
              image={
                listOfImages[i]
                  ? URL.createObjectURL(listOfImages[i])
                  : BLOG_IMAGE
              }
              onDelete={handleDeleteNewMember}
            />
          ))}
        </div>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmitClick}
          className={classes.button}
          disabled={editing}
        >
          Actualizar Información
        </Button>
        <Alert
          open={!!(editSuccess || editError !== '')}
          type={editSuccess ? alertTypes.success : alertTypes.error}
          text={
            editSuccess
              ? 'Se ha actualizado la infomación'
              : 'Error al actualizar la información'
          }
        />
        <CustomBackdrop open={editing} />
      </div>
    </div>
  );
}

const mapState = state => ({
  blogs: fromReducers.getBlogsInfo(state),
  isLoading: state.schoolInfo.isLoading,
  editSuccess: state.schoolInfo.blogs_success,
  editError: state.schoolInfo.blogs_error,
  editing: state.schoolInfo.blogs_isLoading
});

const mapDispatch = dispatch => ({
  editBlogs: values => dispatch(schoolInfoActions.editSIBlogs(values))
});

export default withStyles(styles, { withTheme: true })(
  connect(mapState, mapDispatch)(SIFContact)
);
