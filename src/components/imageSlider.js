import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

import Wait from './common/Wait';
import {
  truncateString,
  SHORT_DESC_MAX_CHARACTERS,
  SHORT_NAME_MAX_CHARACTERS
} from '../utils/strings';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const styles = theme => ({
  slider: {
    width: '100%',
    '&>div': {
      width: 'inherit'
    }
  },
  container: {
    width: '100%'
  },
  waitContainer: {
    width: '100%',
    height: 500
  },
  swipeable: {
    position: 'relative',
    width: '100%',
    display: 'block'
  },
  img: {
    height: 500,
    display: 'block',
    objectFit: 'cover',
    overflow: 'hidden',
    width: '100%'
  },
  infoContainer: {
    position: 'absolute',
    left: '0px',
    bottom: '0px',
    margin: 0,
    width: 'calc(100% - 0px)',
    boxSizing: 'border-box',
    padding: '10px',
    backgroundColor: 'rgba(0,0,0, 0.4)'
  },
  title: {
    margin: '5px 0',
    color: theme.palette.primary.text
  },
  shortDescription: {
    margin: '5px 0',
    color: theme.palette.primary.text
  },
  button: {
    margin: '5px 0',
    backgroundColor: 'rgba(17,1,51, 0.4)'
  }
});

class ImageSlider extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      activeStep: 0
    };
  }

  handleStepChange = activeStep => {
    this.setState({ activeStep });
  };

  render() {
    const { classes, theme, eventList } = this.props;
    const { activeStep } = this.state;
    const eventsToShow = eventList.slice(0, 5);
    if (eventList.length === 0) {
      return (
        <div className={classes.waitContainer}>
          <Wait />
        </div>
      );
    }
    return (
      <AutoPlaySwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={activeStep}
        onChangeIndex={this.handleStepChange}
        enableMouseEvents
        className={classes.slider}
      >
        {eventsToShow.map((event, index) => (
          <div key={`${event.title}-${index}`} className={classes.container}>
            {Math.abs(activeStep - index) <= 2 ? (
              <div className={classes.swipeable}>
                <img
                  className={classes.img}
                  src={event.img + `#${new Date()}`}
                  alt={event.title}
                />
                <div className={classes.infoContainer}>
                  <Typography
                    className={classes.title}
                    variant="h5"
                    color="inherit"
                    // noWrap
                  >
                    {truncateString(event.title, SHORT_NAME_MAX_CHARACTERS)}
                  </Typography>
                  <Typography
                    className={classes.shortDescription}
                    variant="body1"
                    color="inherit"
                    // noWrap
                  >
                    {truncateString(
                      event.bannerAbstract,
                      SHORT_DESC_MAX_CHARACTERS
                    )}
                  </Typography>
                  <Link to={`/event/${event._id}`}>
                    <Button
                      variant="outlined"
                      color="secondary"
                      className={classes.button}
                    >
                      Ver más
                    </Button>
                  </Link>
                </div>
              </div>
            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
    );
  }
}

ImageSlider.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(ImageSlider);
