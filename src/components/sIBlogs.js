import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import ButtonBase from '@material-ui/core/ButtonBase';

import { truncateString, SHORT_NAME_MAX_CHARACTERS } from '../utils/strings';
import * as fromReducers from '../reducers/index';
import Wait from './common/Wait';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  cardsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    width: '100%'
  },
  title: { margin: '30px', alignSelf: 'center' },
  paper: {
    alignItems: 'center',
    marginBottom: theme.spacing(1),
    display: 'flex',
    height: 'max-content',
    minHeight: '100px',
    margin: '10px',
    padding: 0,
    boxSizing: 'border-box'
  },
  cardAction: {
    padding: theme.spacing(1),
    display: 'flex',
    width: '100%',
    justifyContent: 'start'
  },
  cardImage: {
    width: 80,
    height: 80
  },
  cardContent: {
    width: 'auto',
    margin: '2px 10px',
    display: 'flex'
  },
  cardTitle: {
    color: theme.palette.primary.main
  },
  divider: {
    margin: theme.spacing(2, 0)
  },
  link: {
    textDecoration: 'none',
    width: '100%',
    color: theme.palette.primary.text
  }
});

function SIProfessors(props) {
  const { blogs, classes } = props;
  if (!blogs) return <Wait text="Se está cargando la información" />;

  return (
    <div className={classes.container}>
      <div className={classes.cardsContainer}>
        {blogs.map((blog, i) => (
          <a href={blog.link} className={classes.link}>
            <Paper
              key={`blog-${blog.id}-${i}`}
              variant="outlined"
              square
              className={classes.paper}
              zeroMinWidth
            >
              <ButtonBase
                className={props.classes.cardAction}
                onClick={event => {}}
              >
                <Avatar
                  alt="Remy Sharp"
                  src={blog.pictureUrl + `#${new Date()}`}
                  className={classes.cardImage}
                />
                <div className={classes.cardContent}>
                  <Typography variant="h4" className={classes.cardTitle}>
                    {truncateString(blog.name, SHORT_NAME_MAX_CHARACTERS)}
                  </Typography>
                </div>
              </ButtonBase>
            </Paper>
          </a>
        ))}
      </div>
    </div>
  );
}
const mapState = state => ({
  blogs: fromReducers.getBlogsInfo(state)
});

export default connect(mapState)(
  withStyles(styles, { useTheme: true })(SIProfessors)
);
