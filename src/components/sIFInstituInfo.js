import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import * as schoolInfoActions from '../actions/schoolInfo';
import * as fromReducers from '../reducers';
import Alert, { alertTypes } from '../components/common/alert';
import Info from '../components/common/Info';
import Wait from '../components/common/Wait';
import ImageSelect from '../components/common/imageSelect';
import ListItemCreator from '../components/common/listItemCreator';
import ExpandableCampusList from '../components/common/expandableCampusList';
import CustomBackdrop from '../components/common/customBackdrop';

const styles = theme => ({
  container: {
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
    boxSizing: 'border-box'
  },
  title: {
    width: '100%',
    padding: '40px 10px',
    boxSizing: 'border-box',
    textAlign: 'center'
  },
  content: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    padding: '20px',
    boxSizing: 'border-box'
  },
  column: {
    flex: 1,
    display: 'flex',
    minWidth: '200px',
    flexDirection: 'column',
    padding: '10px',
    justifyContent: 'space-between'
  },
  images: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'space-evenly'
  },
  textInput: {
    margin: theme.spacing(1)
  },
  button: {
    width: 'max-content',
    alignSelf: 'center',
    margin: '20px 10px 0px 10px'
  }
});

const entryFields = [
  { name: 'name', label: 'Nombre' },
  { name: 'address', label: 'Dirección' },
  { name: 'description', label: 'Descripción' }
];

function SIFContact(props) {
  const {
    classes,
    instituInfo,
    isLoading,
    editSIInstituInfo,
    editSuccess,
    editError,
    editing
  } = props;
  const [mission, setMission] = useState('');
  const [vision, setVision] = useState('');
  const [anthem, setAnthem] = useState('');
  const [historicalReview, setHistoricalReview] = useState('');
  const [shieldImage, setShiledImage] = useState(null);
  const [flagImage, setFlagImage] = useState(null);
  const [campusDescription, setCampusDescription] = useState('');
  const [campusList, setCampusList] = useState([]);

  useEffect(() => {
    if (instituInfo) {
      setMission(instituInfo.mission);
      setVision(instituInfo.vision);
      setAnthem(instituInfo.anthem);
      setHistoricalReview(instituInfo.historicalReview);
      setCampusDescription(instituInfo.campus.description);
      setCampusList(instituInfo.campus.list);
    }
  }, [instituInfo]);

  const handleSubmitClick = () => {
    const data = {
      mission,
      vision,
      historicalReview,
      anthem,
      campus: {
        description: campusDescription,
        list: campusList
      }
    };
    const fData = new FormData();
    fData.append('data', JSON.stringify(data));
    if (shieldImage) fData.append('symbols', shieldImage, 'shield');
    if (flagImage) fData.append('symbols', flagImage, 'flag');
    editSIInstituInfo(fData);
  };

  const handleCampusCreation = entry => {
    setCampusList([...campusList, entry]);
  };

  const handleDeleteCampusEntry = position => {
    setCampusList(campusList.filter((item, i) => i !== position));
  };

  if (!instituInfo && isLoading)
    return <Wait text="Se está cargando la información Instituciónal" />;

  if (!instituInfo)
    return <Info text="No se ha cargado la información Institucional" />;
  return (
    <div>
      <div className={classes.container}>
        <Typography variant="h4" component="h6" className={classes.title}>
          Editar información institucional
        </Typography>
        <div className={classes.content}>
          <div className={classes.column}>
            <TextField
              name="mission"
              label="Misión"
              multiline
              rows="4"
              value={mission}
              onChange={e => setMission(e.target.value)}
              className={classes.textInput}
            />
            <TextField
              name="vision"
              label="Visión"
              multiline
              rows="4"
              value={vision}
              onChange={e => setVision(e.target.value)}
              className={classes.textInput}
            />
            <TextField
              name="historicalReview"
              label="Reseña historica"
              multiline
              rows="4"
              value={historicalReview}
              onChange={e => setHistoricalReview(e.target.value)}
              className={classes.textInput}
            />
          </div>
          <div className={classes.column}>
            <div className={classes.images}>
              <ImageSelect
                name="shield"
                label="Escudo"
                onChange={setShiledImage}
                value={shieldImage}
                defaultValue={instituInfo.shieldUrl + `#${new Date()}`}
              />
              <ImageSelect
                name="flag"
                label="Bandera"
                onChange={setFlagImage}
                value={flagImage}
                defaultValue={instituInfo.flagUrl + `#${new Date()}`}
              />
            </div>
            <TextField
              name="anthem"
              label="Himno"
              multiline
              rows="4"
              value={anthem}
              onChange={e => setAnthem(e.target.value)}
              className={classes.textInput}
            />
            <TextField
              name="campusDescription"
              label="Descripción de las sedes"
              multiline
              rows="4"
              value={campusDescription}
              onChange={e => setCampusDescription(e.target.value)}
              className={classes.textInput}
            />
          </div>
        </div>
        <ListItemCreator
          label="Crear nueva sede"
          entryFields={entryFields}
          onSubmit={handleCampusCreation}
        />
        <ExpandableCampusList
          onDelete={handleDeleteCampusEntry}
          list={campusList}
          label="Lista de sedes"
        />
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmitClick}
          className={classes.button}
          disabled={editing}
        >
          Actualizar Información
        </Button>
        <Alert
          open={!!(editSuccess || editError !== '')}
          type={editSuccess ? alertTypes.success : alertTypes.error}
          text={
            editSuccess
              ? 'Se ha actualizado la infomación'
              : 'Error al actualizar la información'
          }
        />
        <CustomBackdrop open={editing} />
      </div>
    </div>
  );
}

const mapState = state => ({
  instituInfo: fromReducers.getInstitutionalInfo(state),
  isLoading: state.schoolInfo.isLoading,
  editSuccess: state.schoolInfo.instituInfo_success,
  editError: state.schoolInfo.instituInfo_error,
  editing: state.schoolInfo.instituInfo_isLoading
});

const mapDispatch = dispatch => ({
  editSIInstituInfo: values =>
    dispatch(schoolInfoActions.editSIInstituInfo(values))
});

export default withStyles(styles, { withTheme: true })(
  connect(mapState, mapDispatch)(SIFContact)
);
