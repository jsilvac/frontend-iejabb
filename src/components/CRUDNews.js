import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles, useTheme } from '@material-ui/core/styles';

import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Form from './form/index';
import { TabPanel, a11yProps } from './common/tabs';
import { createNews, fetchNews } from '../actions/news';
import NewsContainerSmall from './newsContainerSmall';
import { newsScheme } from './form/schemes';
import Alert, { alertTypes } from './common/alert';
import CustomBackdrop from './common/customBackdrop';

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    maxWidth: '600px',
    minHeight: '600px'
  }
});

function CRUDNews(props) {
  const {
    classes,
    onSubmitCreate,
    fetchNewsList,
    createError,
    createSuccess,
    creating,
    editing
  } = props;
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  useEffect(() => {
    fetchNewsList();
  }, [fetchNewsList]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = index => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Lista" {...a11yProps(0)} />
          <Tab label="Añadir" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <h3>Noticias</h3>
          <NewsContainerSmall stepperOn nItemsPStep={6} />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <h3>Agregar noticia</h3>
          <Form
            scheme={newsScheme}
            onSubmitForm={onSubmitCreate}
            identifier="create-news-form"
            reset={createSuccess}
          />
        </TabPanel>
      </SwipeableViews>
      <CustomBackdrop open={creating || editing} />
      <Alert
        open={!!(createSuccess || createError !== '')}
        type={createSuccess ? alertTypes.success : alertTypes.error}
        text={
          createSuccess
            ? 'Se ha creado la noticia'
            : 'Hubo un error al crear la noticia'
        }
      />
    </div>
  );
}

const mapState = state => ({
  creating: state.news.create_isLoading,
  editing: state.news.edit_isLoading,
  createError: state.news.create_error,
  createSuccess: state.news.create_success
});

const mapDispatch = dispatch => ({
  onSubmitCreate: values => {
    dispatch(createNews(values));
  },
  fetchNewsList: () => dispatch(fetchNews())
});

export default connect(mapState, mapDispatch)(withStyles(styles)(CRUDNews));
