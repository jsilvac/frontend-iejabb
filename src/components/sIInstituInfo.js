import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

import * as fromReducers from '../reducers/index';

import SimplePaperArticle from './common/simplePaperArticle';
import Wait from '../components/common/Wait';

const styles = theme => ({
  mainContainer: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    width: '100%'
  },
  upperSectionContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'space-between',
    alignContent: 'baseline',
    [theme.breakpoints.down(1010)]: {
      width: '100%',
      justifyContent: 'center'
    }
  },
  bottomSectionContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'center',
    alignContent: 'baseline'
  },
  symbols: {
    boxSizing: 'border-box',
    width: '56%',
    display: 'flex',
    flexDirection: 'column',
    padding: '10px 10px 10px 10px',
    margin: '10px',
    [theme.breakpoints.down(1010)]: {
      width: '100%'
    }
  },
  symbolsTitle: {
    width: '100%',
    textAlign: 'center'
  },
  symbolsContainer: {
    width: '100%',
    display: 'flex',
    padding: '20px',
    [theme.breakpoints.down(750)]: {
      flexDirection: 'column'
    }
  },
  symbolTitle: {
    width: '100%',
    textAlign: 'center'
  },
  flagAndShieldContainer: {
    minWidht: '100px',
    width: '20%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    [theme.breakpoints.down(750)]: {
      flexDirection: 'row',
      width: '100%',
      marginBottom: '20px'
    }
  },
  shieldContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  shieldImage: {
    width: '100px',
    height: '100px'
  },
  flagContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  flagImage: {
    width: '100px',
    height: '100px'
  },
  anthemContainer: {
    flexGrow: '1',
    display: 'flex',
    flexDirection: 'column',
    maxHeight: '500px',
    overflow: 'auto'
  },
  anthemBody: {
    whiteSpace: 'pre-line',
    textAlign: 'center'
  },
  campusContainer: {
    width: '40%',
    boxSizing: 'border-box',
    display: 'flex',
    flexDirection: 'column',
    padding: '15px',
    margin: '10px',

    maxHeight: '600px',
    minWidth: '300px',
    [theme.breakpoints.down(1010)]: {
      width: '100%'
    }
  },
  campusTitle: {
    width: '100%',
    textAlign: 'center'
  },
  campusBody: {
    whiteSpace: 'pre-line',
    margin: '5px 0'
  },
  campusList: {
    overflow: 'auto'
  },
  campusListItem: {},
  campusListItemDescription: {
    color: theme.palette.secondary.text
  }
});

function SIInstituInfo(props) {
  const { institutionalInfo, classes } = props;
  if (!institutionalInfo)
    return <Wait text="Se está cargando la información" />;

  return (
    <div className={classes.mainContainer}>
      <div className={classes.upperSectionContainer}>
        <Paper className={classes.symbols} square>
          <Typography variant="h6" className={classes.symbolsTitle}>
            Simbolos institucionales
          </Typography>
          <Divider />
          <div className={classes.symbolsContainer}>
            <div className={classes.flagAndShieldContainer}>
              <div className={classes.flagContainer}>
                <Typography variant="h6" className={classes.symbolTitle}>
                  Bandera
                </Typography>
                <Avatar
                  alt="Bandera"
                  src={institutionalInfo.flagUrl + `#${new Date()}`}
                  className={classes.flagImage}
                />
              </div>
              <div className={classes.shieldContainer}>
                <Typography variant="h6" className={classes.symbolTitle}>
                  Escudo
                </Typography>
                <Avatar
                  alt="Escudo"
                  src={institutionalInfo.shieldUrl + `#${new Date()}`}
                  className={classes.shieldImage}
                />
              </div>
            </div>

            <div className={classes.anthemContainer}>
              <Typography variant="h6" className={classes.symbolTitle}>
                Himno
              </Typography>
              <Typography variant="body1" className={classes.anthemBody}>
                {institutionalInfo.anthem}
              </Typography>
            </div>
          </div>
        </Paper>
        <Paper className={classes.campusContainer} square>
          <Typography variant="h6" className={classes.symbolTitle}>
            Sedes
          </Typography>
          <Typography variant="body1" className={classes.campusBody}>
            {institutionalInfo.campus.description}
          </Typography>
          <Typography variant="h6" className={classes.symbolTitle}>
            Lista de Sedes:
          </Typography>
          <div className={classes.campusList}>
            {institutionalInfo.campus.list.map((campus, i) => (
              <div
                className={classes.campusListItem}
                key={`${campus.name}-${i}`}
              >
                <Typography variant="body1" className={classes.campusBody}>
                  {campus.name}
                </Typography>
                <Typography variant="body2" className={classes.campusBody}>
                  {`Dirección: ${campus.address}`}
                </Typography>
                <Typography
                  variant="body2"
                  className={classes.campusListItemDescription}
                >
                  {`${campus.description}`}
                </Typography>
                {i !== institutionalInfo.campus.list.length - 1 ? (
                  <Divider />
                ) : null}
              </div>
            ))}
          </div>
        </Paper>
      </div>
      <div className={classes.bottomSectionContainer}>
        <SimplePaperArticle title="Misión" body={institutionalInfo.mission} />
        <SimplePaperArticle title="vision" body={institutionalInfo.vision} />
        <SimplePaperArticle
          title="Reseña Historica"
          body={institutionalInfo.historicalReview}
        />
      </div>
    </div>
  );
}
const mapState = state => ({
  institutionalInfo: fromReducers.getInstitutionalInfo(state)
});

export default connect(mapState)(withStyles(styles)(SIInstituInfo));
