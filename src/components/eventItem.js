import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

import newsImage from '../assets/images/current-events-512.png';
import {
  truncateString,
  SHORT_DESC_MAX_CHARACTERS,
  TINY_DESC_MAX_CHARACTERS
} from '../utils/strings';
import { formatDate } from '../utils/date';

const styles = theme => ({
  card: {
    width: '300px',
    height: '420px',
    margin: '10px',
    position: 'relative'
  },
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  buttonSeeMore: {
    position: 'absolute',
    bottom: 0,
    padding: '8px 15px',
    boxSizing: 'border-box',
    display: 'flex',
    width: '100%',
    justifyContent: 'end'
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: theme.palette.primary.dark
  }
});

class Event extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = { expanded: false };
  }

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    const { classes, data } = this.props;
    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="event" className={classes.avatar}>
              E
            </Avatar>
          }
          title={truncateString(data.title, TINY_DESC_MAX_CHARACTERS)}
          subheader={formatDate(data.publishedDate)}
        />
        <CardMedia
          className={classes.media}
          image={
            data.img && data.img !== ''
              ? data.img + `#${new Date()}`
              : newsImage
          }
          title="image"
        />
        <CardContent>
          <Typography component="p">
            {truncateString(data.content, SHORT_DESC_MAX_CHARACTERS)}
          </Typography>
        </CardContent>
        <CardActions className={classes.buttonSeeMore}>
          <Link to={`/event/${data._id}`}>
            <Button size="small">Más información</Button>
          </Link>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(styles)(Event);
