import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import { useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import usePrevious from '../hooks/usePrevious';
import EventItem from './eventItem';
import Wait from './common/Wait';
import Info from './common/Info';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    margin: '10px',
    flexWrap: 'wrap'
  },
  stepper: {
    width: '100%',
    flexGrow: 1
  }
});

function EventContainer(props) {
  const { classes, eventList, loading, stepperOn, nItemsPStep } = props;
  const [activeStep, setActiveStep] = useState(0);
  const theme = useTheme();
  const prevEventList = usePrevious(eventList);

  let nSteps = 0;
  let eventsToShow = eventList;
  useEffect(() => {
    if (prevEventList && prevEventList.length !== eventList.length) {
      setActiveStep(0);
    }
  }, [prevEventList, eventList]);

  if (loading) return <Wait text="Se están cargando los eventos" />;
  if (eventList.length === 0)
    return <Info text="No se han encontrado eventos" />;
  if (!stepperOn) {
    eventsToShow = eventList.slice(0, nItemsPStep || 6);
    return (
      <div className={classes.container}>
        {eventsToShow.map((news, i) => (
          <EventItem key={news._id} data={news} />
        ))}
      </div>
    );
  } else if (stepperOn && nItemsPStep) {
    eventsToShow = eventList.slice(
      nItemsPStep * activeStep,
      (activeStep + 1) * nItemsPStep
    );
    nSteps = !!(eventList.length % nItemsPStep)
      ? parseInt(eventList.length / nItemsPStep) + 1
      : parseInt(eventList.length / nItemsPStep);
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  return (
    <div>
      <div className={classes.container}>
        {eventsToShow.map((news, i) => (
          <EventItem key={news._id} data={news} />
        ))}
      </div>
      <MobileStepper
        variant="text"
        steps={nSteps}
        position="static"
        activeStep={activeStep}
        className={classes.stepper}
        nextButton={
          <Button
            size="small"
            onClick={handleNext}
            disabled={activeStep === nSteps - 1}
          >
            Siguiente
            {theme.direction === 'rtl' ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {theme.direction === 'rtl' ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
            Anterior
          </Button>
        }
      />
    </div>
  );
}

export default withStyles(styles)(EventContainer);
