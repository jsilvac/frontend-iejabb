import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme => ({
  textButton: {
    color: theme.palette.primary.text,
    backgroundColor: theme.palette.primary.main,
    [theme.breakpoints.down(700)]: {
      display: 'none'
    }
  },
  iconButton: {
    color: theme.palette.primary.text,
    backgroundColor: theme.palette.primary.main,
    [theme.breakpoints.up(700)]: {
      display: 'none'
    }
  }
}));

export default function HeaderButton(props) {
  const { text, Icon } = props;
  const classes = useStyles();
  return (
    <>
      <Button color="inherit" className={classes.textButton}>
        {text}
      </Button>
      <Tooltip title={text} aria-label={text}>
        <IconButton className={classes.iconButton}>
          <Icon />
        </IconButton>
      </Tooltip>
    </>
  );
}
