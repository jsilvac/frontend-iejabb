import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';

import * as newsActions from '../actions/news';
import AlertButton from './common/alertButton';
import FormButton from './common/formButton';
import USER_IMAGE from '../assets/images/user.png';
import { newsScheme } from './form/schemes';
import { truncateString, TINY_DESC_MAX_CHARACTERS } from '../utils/strings';

const useStyles = makeStyles(theme => ({
  inline: {
    display: 'inline'
  }
}));

function NewsItemSmall(props) {
  const classes = useStyles();
  const {
    newsInfo,
    deleteNews,
    editNews,
    editError,
    editSuccess,
    editing
  } = props;

  const handleDelete = () => {
    deleteNews(newsInfo._id);
  };

  const handleEdit = newContent => {
    editNews(newsInfo._id, newContent);
  };

  return (
    <ListItem alignItems="center">
      <ListItemAvatar>
        <Avatar
          alt="Cindy Baker"
          src={newsInfo.img ? `${newsInfo.img}?${Date.now()}` : USER_IMAGE}
        />
      </ListItemAvatar>
      <ListItemText
        primary={truncateString(newsInfo.title, TINY_DESC_MAX_CHARACTERS)}
        secondary={
          <React.Fragment>
            <Typography
              component="span"
              variant="body2"
              className={classes.inline}
              color="textPrimary"
            >
              {newsInfo.author}
            </Typography>
            {` - ${truncateString(newsInfo.content, TINY_DESC_MAX_CHARACTERS)}`}
          </React.Fragment>
        }
      />
      <FormButton
        onSubmit={handleEdit}
        scheme={newsScheme}
        initialContent={newsInfo}
        title="Editar noticia"
        identifier="edit-news-form"
        reset={editSuccess}
        formError={editError}
        formSuccess={editSuccess}
        formLoading={editing}
        textSuccess="Se ha editado la noticia"
        textError="Hubo un error al editar la noticia"
      >
        <EditIcon />
      </FormButton>
      <AlertButton
        onAgree={handleDelete}
        title="Eliminar noticia"
        content="¿Desea eliminar esta noticia?"
      >
        <DeleteForeverIcon />
      </AlertButton>
    </ListItem>
  );
}

const mapState = state => ({
  editing: state.news.edit_isLoading,
  editError: state.news.edit_error,
  editSuccess: state.news.edit_success
});

const mapDispatch = dispatch => ({
  deleteNews: id => dispatch(newsActions.deleteNews(id)),
  editNews: (id, content) => dispatch(newsActions.editNews(id, content)),
  resetSuccess: () => dispatch(newsActions.resetNewsSuccess()),
  resetError: () => dispatch(newsActions.resetNewsError())
});

export default connect(mapState, mapDispatch)(NewsItemSmall);
