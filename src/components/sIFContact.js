import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import * as schoolInfoActions from '../actions/schoolInfo';
import * as fromReducers from '../reducers';
import Alert, { alertTypes } from '../components/common/alert';
import Info from '../components/common/Info';
import Wait from '../components/common/Wait';
import CustomBackdrop from '../components/common/customBackdrop';

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  title: {
    flex: '1',
    margin: '40px 10px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '3rem',
      textAlign: 'center'
    }
  },
  form: {
    flex: '1',
    minWidth: '260px',
    display: 'flex',
    flexDirection: 'column',
    margin: '40px'
  },
  textInput: {
    margin: theme.spacing(1)
  },
  button: {
    width: 'max-content',
    alignSelf: 'center',
    margin: '20px 10px 0px 10px'
  }
});

const inputFields = [
  {
    label: 'Nombre de la escuela',
    name: 'schoolName'
  },
  {
    label: 'Dirección',
    name: 'address'
  },
  {
    label: 'Teléfono',
    name: 'phone'
  },
  {
    label: 'Departamento',
    name: 'state'
  },
  {
    label: 'Ciudad',
    name: 'city'
  },
  {
    label: 'Página de Facebook',
    name: 'facebook'
  },
  {
    label: 'Página de twitter',
    name: 'twitter'
  },
  {
    label: 'Usuario de twitter para mostrar en embebido',
    name: 'twitterEmbeded'
  },
  {
    label: 'Página Instagram',
    name: 'instagram'
  },
  {
    label: 'Página de Plataforma',
    name: 'platform'
  },
  {
    label: 'Coordenada de ubicación Latitud',
    name: 'latitude'
  },
  {
    label: 'Coordenada de ubicación Longitud',
    name: 'longitude'
  }
];

function SIFContact(props) {
  const {
    classes,
    contactInfo,
    isLoading,
    editSIContact,
    editSuccess,
    editError,
    editing
  } = props;
  const [values, setValues] = useState({
    schoolName: '',
    address: '',
    phone: '',
    state: '',
    city: '',
    facebook: '',
    twitter: '',
    twitterEmbeded: '',
    instagram: '',
    platform: '',
    latitude: '',
    longitude: ''
  });

  useEffect(() => {
    if (contactInfo) {
      setValues({
        schoolName: contactInfo.schoolName,
        address: contactInfo.address,
        phone: contactInfo.phone,
        state: contactInfo.state,
        city: contactInfo.city,
        facebook: contactInfo.facebook,
        twitter: contactInfo.twitter,
        twitterEmbeded: contactInfo.twitterEmbeded,
        instagram: contactInfo.instagram,
        platform: contactInfo.platform,
        latitude: contactInfo.latitude,
        longitude: contactInfo.longitude
      });
    }
  }, [contactInfo]);

  const handleChangeValue = event => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmitClick = () => {
    editSIContact(values);
  };

  if (!contactInfo && isLoading)
    return <Wait text="Se está cargando la información de contacto" />;

  if (!contactInfo)
    return <Info text="No se ha cargado la información de contacto" />;

  return (
    <div>
      <div className={classes.container}>
        <Typography variant="h1" component="h2" className={classes.title}>
          Editar información de contacto
        </Typography>
        <div className={classes.form}>
          {inputFields.map((field, i) => (
            <TextField
              key={`contact-${field}-${i}`}
              name={field.name}
              label={field.label}
              value={values[field.name]}
              onChange={handleChangeValue}
              className={classes.textInput}
            />
          ))}
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmitClick}
            className={classes.button}
            disabled={editing}
          >
            Actualizar Información
          </Button>
        </div>
        <Alert
          open={!!(editSuccess || editError !== '')}
          type={editSuccess ? alertTypes.success : alertTypes.error}
          text={
            editSuccess
              ? 'Se ha actualizado la petición'
              : 'Error al actualizar'
          }
        />
        <CustomBackdrop open={editing} />
      </div>
    </div>
  );
}

const mapState = state => ({
  contactInfo: fromReducers.getContactInfo(state),
  isLoading: state.schoolInfo.isLoading,
  editSuccess: state.schoolInfo.contact_success,
  editError: state.schoolInfo.contact_error,
  editing: state.schoolInfo.contact_isLoading
});

const mapDispatch = dispatch => ({
  editSIContact: values => dispatch(schoolInfoActions.editSIContact(values))
});

export default withStyles(styles, { withTheme: true })(
  connect(mapState, mapDispatch)(SIFContact)
);
