import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    width: '350px',
    minHeight: '180px',
    height: 'auto',
    margin: '10px',
    boxSizing: 'border-box'
  },
  details: {
    width: '60%',
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    flex: '1 0 auto'
  },
  picture: {
    width: '40%',
    height: '100%'
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  playIcon: {
    height: 38,
    width: 38
  }
}));

export default function MemberCard(props) {
  const { id, name, position, extraText, image, onDelete } = props;
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography component="h6" variant="h6">
            {name}
          </Typography>
          <Typography variant="subtitle2" color="textSecondary">
            {position}
          </Typography>
          {extraText ? (
            <Typography variant="body2" color="textSecondary">
              {extraText}
            </Typography>
          ) : null}
        </CardContent>
        {onDelete ? (
          <CardActions>
            <Button size="small" onClick={() => onDelete(id)}>
              Eliminar
            </Button>
          </CardActions>
        ) : null}
      </div>
      <CardMedia
        key={`${image}?${Date.now()}`}
        className={classes.picture}
        image={image}
        title={name}
      />
    </Card>
  );
}
