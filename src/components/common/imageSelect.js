import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  image: {
    width: '100px',
    height: '100px',
    margin: '10px'
  },
  button: {
    margin: '10px'
  }
});

function ImageSelect(props) {
  const { classes, onChange, value, defaultValue, label, name } = props;
  return (
    <div className={classes.container}>
      {label ? <Typography variant="h4">{label}</Typography> : null}
      <Avatar
        alt={`${name}_image_input`}
        src={value ? URL.createObjectURL(value) : defaultValue}
        className={classes.image}
      />
      <input
        accept="image/*"
        style={{ display: 'none' }}
        id={`${name}_image_input`}
        multiple
        type="file"
        onChange={e => onChange(e.target.files[0])}
      />
      <label htmlFor={`${name}_image_input`}>
        <Button
          variant="contained"
          color="default"
          component="span"
          className={classes.button}
          startIcon={<CloudUploadIcon />}
        >
          Seleccionar Imágen
        </Button>
      </label>
    </div>
  );
}

export default withStyles(styles, { withTheme: true })(ImageSelect);
