import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { makeStyles } from '@material-ui/core/styles';
import CustomBackdrop from './customBackdrop';

import IconButton from '@material-ui/core/IconButton';

import Form from '../form';
import Alert, { alertTypes } from './alert';

const useStyles = makeStyles(theme => ({
  title: {
    color: theme.palette.primary.text,
    backgroundColor: theme.palette.primary.main
  }
}));

export default function FormButton(props) {
  const {
    children,
    onSubmit,
    scheme,
    initialContent,
    title,
    identifier,
    formError,
    formSuccess,
    formLoading,
    textSuccess,
    textError
  } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = content => {
    onSubmit(content);
  };

  return (
    <div>
      <IconButton aria-label="edit" onClick={handleClickOpen}>
        {children}
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        fullWidth
      >
        <DialogTitle id="form-dialog-title" className={classes.title}>
          {title}
        </DialogTitle>
        <DialogContent>
          <Form
            scheme={scheme}
            onSubmitForm={handleSubmit}
            initialContent={initialContent}
            identifier={identifier}
            reset={false}
          />
        </DialogContent>
        <CustomBackdrop open={formLoading} />
        <Alert
          open={!!(formSuccess || formError !== '')}
          type={formSuccess ? alertTypes.success : alertTypes.error}
          text={formSuccess ? textSuccess : textError}
        />
      </Dialog>
    </div>
  );
}
