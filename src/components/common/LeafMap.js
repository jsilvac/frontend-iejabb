import React, { Component } from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  map: {
    width: '100%',
    height: '30em',
    minWidth: '400px'
  },
  mTextPrimary: {
    color: theme.palette.primary.main
  },
  mTextSecondary: {
    color: theme.palette.secondary.main
  }
});

class LeafMap extends Component {
  render() {
    const { classes, lat, lng, zoom, textPrimary, textSecondary } = this.props;
    const position = [lat, lng];
    return (
      <Map center={position} className={classes.map} zoom={zoom}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
        />
        <Marker position={position}>
          <Popup>
            <span className={classes.mTextPrimary}>{textPrimary}</span>
            <br />
            <span className={classes.mTextSecondary}>{textSecondary}</span>
          </Popup>
        </Marker>
      </Map>
    );
  }
}

export default withStyles(styles)(LeafMap);
