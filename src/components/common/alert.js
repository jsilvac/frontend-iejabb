import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { withStyles } from '@material-ui/core/styles';

export const alertTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warning: 'warning'
};

const styles = theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.background.paper
    }
  },
  snackbar: {
    position: 'fixed',
    bottom: 40,
    [theme.breakpoints.down('xs')]: {
      bottom: 90
    }
  }
});

class Alert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { open, seconds } = this.props;
    if (open && open !== prevProps.open) {
      setTimeout(
        () => {
          this.setState(prevState => {
            return { ...prevState, isOpen: false };
          });
        },
        seconds ? seconds * 1000 : 5000
      );
      this.setState(prevState => {
        return { ...prevState, isOpen: true };
      });
    }
  }

  handleOnClose = () => {
    this.setState(prevState => {
      return { ...prevState, isOpen: false };
    });
  };

  render() {
    const { type, text, classes, verticalPos, horizontalPos } = this.props;
    const { isOpen } = this.state;
    return (
      <Snackbar
        open={isOpen}
        anchorOrigin={{
          vertical: verticalPos || 'bottom',
          horizontal: horizontalPos || 'center'
        }}
        className={classes.snackbar}
        autoHideDuration={6000}
        onClose={this.handleOnClose}
      >
        <MuiAlert
          elevation={8}
          variant="filled"
          onClose={this.handleOnClose}
          severity={type}
        >
          {text}
        </MuiAlert>
      </Snackbar>
    );
  }
}

export default withStyles(styles)(Alert);
