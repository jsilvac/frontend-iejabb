import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  image: {
    width: '100px',
    height: '100px',
    margin: '10px'
  },
  button: {
    margin: '10px'
  },
  textInput: {
    minWidth: '200px'
  }
});

function ListItemCreator(props) {
  const { classes, onSubmit, entryFields, label } = props;
  const [entry, setEntry] = useState({});

  const handleOnChangeField = (name, value) => {
    setEntry({
      ...entry,
      [name]: value
    });
  };

  const handleSubmit = () => {
    let isValidEntry = true;
    entryFields.forEach(field => {
      if (!entry[field.name]) {
        isValidEntry = false;
      }
    });
    if (isValidEntry) {
      onSubmit(entry);
      setEntry({});
    }
  };

  return (
    <div className={classes.container}>
      <Typography variant="h6">{label}</Typography>
      {entryFields.map((field, i) => (
        <TextField
          key={`${field.name}-${field.label}-${i}`}
          name={field.name}
          label={field.label}
          value={entry[field.name] || ''}
          onChange={e => handleOnChangeField(field.name, e.target.value)}
          className={classes.textInput}
        />
      ))}

      <Button
        variant="contained"
        onClick={handleSubmit}
        className={classes.button}
      >
        Crear
      </Button>
    </div>
  );
}

export default withStyles(styles, { withTheme: true })(ListItemCreator);
