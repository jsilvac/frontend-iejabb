import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 10,
    color: theme.palette.primary.main
  }
}));

export default function CustomBackdrop(props) {
  const classes = useStyles();
  const { open } = props;
  return (
    <Backdrop className={classes.backdrop} open={open}>
      <CircularProgress />
    </Backdrop>
  );
}
