import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    width: '32%',
    minWidth: '300px',
    margin: theme.spacing(1),
    padding: '20px',
    height: 'max-content',
    maxHeight: '900px',
    [theme.breakpoints.down(750)]: {
      width: '100%'
    }
  },
  title: {
    width: '100%',
    textAlign: 'center'
  },
  body: {
    width: '100%',
    whiteSpace: 'pre-line'
  }
}));

export default function SimplePaper(props) {
  const { title, body } = props;
  const classes = useStyles();
  return (
    <Paper square className={classes.paper}>
      <Typography variant="h6" className={classes.title}>
        {title}
      </Typography>
      <Typography variant="body1" className={classes.body}>
        {body}
      </Typography>
    </Paper>
  );
}
