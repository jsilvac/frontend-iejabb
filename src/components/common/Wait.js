import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    width: 'inherit',
    height: '100%'
  },
  progess: {
    margin: 15
  },
  text: {
    margin: 5
  }
});

function Wait(props) {
  const { text, classes } = props;
  return (
    <div className={classes.container}>
      <CircularProgress
        color="secondary"
        size={80}
        className={classes.progess}
      />
      {text ? (
        <Typography variant="h6" className={classes.text}>
          {text}
        </Typography>
      ) : null}
    </div>
  );
}

export default withStyles(styles)(Wait);
