import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    width: 'inherit',
    height: '100%'
  },
  text: {
    margin: 5
  }
});

function Info(props) {
  const { text, classes } = props;
  return (
    <div className={classes.container}>
      {text ? (
        <Typography variant="h6" className={classes.text}>
          {text}
        </Typography>
      ) : null}
    </div>
  );
}

export default withStyles(styles)(Info);
