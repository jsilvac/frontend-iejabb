import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import ImageSelect from './imageSelect';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '80%'
  },
  button: {
    margin: '10px'
  },
  textInput: {
    minWidth: '200px',
    margin: '5px'
  },
  fab: {
    position: 'fixed',
    zIndex: 1,
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      padding: 0
    }
  },
  buttonText: {
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  entries: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  }
});

function MemberCreator(props) {
  const { classes, onSubmit, memberFields, memberType, memberLabel } = props;
  const [openDialog, setOpenDialog] = useState(false);
  const [member, setMember] = useState({});
  const [image, setImage] = useState(null);

  const handleOnChangeField = (name, value) => {
    setMember({
      ...member,
      [name]: value
    });
  };

  const handleClose = () => {
    setOpenDialog(false);
  };

  const handleOpen = () => {
    setOpenDialog(true);
  };

  const handleSubmit = () => {
    let isValidMember = true;
    memberFields.forEach(field => {
      if (!member[field.name]) {
        isValidMember = false;
      }
    });
    if (isValidMember) {
      onSubmit({
        member,
        image
      });
      setMember({});
      setImage(null);
      setOpenDialog(false);
    }
  };

  return (
    <>
      <Fab
        variant="extended"
        color="primary"
        aria-label="add"
        className={classes.fab}
        onClick={handleOpen}
      >
        <AddIcon className={classes.extendedIcon} />
        <p className={classes.buttonText}>{`Crear nuevo ${memberLabel}`}</p>
      </Fab>
      <Dialog
        open={openDialog}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{`Crear nuevo ${memberLabel}`}</DialogTitle>
        <DialogContent>
          <DialogContentText></DialogContentText>
          <ImageSelect
            onChange={setImage}
            value={image}
            name={`${memberType}_picture`}
          />
          <div className={classes.content}>
            <div className={classes.imageSelect}></div>
            <div className={classes.entries}>
              {memberFields.map((field, i) => (
                <TextField
                  key={`${field.name}-${field.label}-${i}`}
                  name={field.name}
                  label={field.label}
                  value={member[field.name] || ''}
                  onChange={e =>
                    handleOnChangeField(field.name, e.target.value)
                  }
                  className={classes.textInput}
                />
              ))}
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Crear
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default withStyles(styles, { withTheme: true })(MemberCreator);
