import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import * as schoolInfoActions from '../actions/schoolInfo';
import * as fromReducers from '../reducers';
import Alert, { alertTypes } from './common/alert';
import Info from './common/Info';
import Wait from './common/Wait';
import CustomBackdrop from '../components/common/customBackdrop';

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  title: {
    flex: '1',
    margin: '40px 10px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '3rem',
      textAlign: 'center'
    }
  },
  form: {
    flex: '1',
    minWidth: '260px',
    display: 'flex',
    flexDirection: 'column',
    margin: '40px'
  },
  textInput: {
    margin: theme.spacing(1)
  },
  button: {
    width: 'max-content',
    alignSelf: 'center',
    margin: '20px 10px 0px 10px'
  }
});

function SIFContact(props) {
  const {
    classes,
    rules,
    isLoading,
    editSIRules,
    editSuccess,
    editError,
    editing
  } = props;
  const [description, setDescription] = useState('');
  const [content, setContent] = useState('');

  useEffect(() => {
    if (rules) {
      setDescription(rules.description);
      setContent(rules.content);
    }
  }, [rules]);

  const handleSubmitClick = () => {
    editSIRules({ description, content });
  };

  if (!rules && isLoading)
    return <Wait text="Se está cargando la reglamentación" />;

  if (!rules) return <Info text="No se ha cargado la reglamentación" />;

  return (
    <div>
      <div className={classes.container}>
        <Typography variant="h1" component="h2" className={classes.title}>
          Editar reglamentación
        </Typography>
        <div className={classes.form}>
          <TextField
            name="description"
            label="Descripción"
            multiline
            rows="6"
            value={description}
            onChange={e => setDescription(e.target.value)}
            className={classes.textInput}
          />
          <TextField
            name="content"
            label="Reglamentación"
            multiline
            rows="10"
            value={content}
            onChange={e => setContent(e.target.value)}
            className={classes.textInput}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmitClick}
            className={classes.button}
            disabled={editing}
          >
            Actualizar Información
          </Button>
        </div>
        <Alert
          open={!!(editSuccess || editError !== '')}
          type={editSuccess ? alertTypes.success : alertTypes.error}
          text={
            editSuccess
              ? 'Se ha enviado la petición'
              : 'Hubo un error al enviar la petición'
          }
        />
        <CustomBackdrop open={editing} />
      </div>
    </div>
  );
}

const mapState = state => ({
  rules: fromReducers.getRules(state),
  isLoading: state.schoolInfo.isLoading,
  editSuccess: state.schoolInfo.rules_success,
  editError: state.schoolInfo.rules_error,
  editing: state.schoolInfo.rules_isLoading
});

const mapDispatch = dispatch => ({
  editSIRules: values => dispatch(schoolInfoActions.editSIRules(values))
});

export default withStyles(styles, { withTheme: true })(
  connect(mapState, mapDispatch)(SIFContact)
);
