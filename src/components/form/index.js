import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import INPUT_TYPE from './inputTypes';

const styles = theme => ({
  container: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column'
  },
  button: {
    margin: '10px',
    padding: '5px 20px',
    alignSelf: 'center',
    width: 'max-content'
  }
});

class Form extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      inputs: {},
      errors: [],
      reset: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { scheme, initialContent } = this.props;
    const initialInputs = {};
    if (initialContent) {
      scheme.inputs.forEach((input, i) => {
        if (scheme.inputTypes[i] === 'image') {
          initialInputs[input] = null;
        } else {
          initialInputs[input] = initialContent[input];
        }
      });
    } else {
      scheme.inputs.forEach((input, i) => {
        if (scheme.inputTypes[i] === 'date') {
          initialInputs[input] = new Date();
        } else {
          initialInputs[input] = null;
        }
      });
    }
    this.setState(prevState => ({
      ...prevState,
      inputs: initialInputs
    }));
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { reset } = this.props;
    if (reset && reset !== prevProps.reset) {
      this.resetForm();
      this.setState(prevState => {
        return { reset: true, ...prevState };
      });
    }
  }

  resetForm() {
    const { scheme } = this.props;
    const initialInputs = {};
    scheme.inputs.forEach((input, i) => {
      initialInputs[input] = null;
    });
    this.setState(prevState => ({
      ...prevState,
      inputs: initialInputs
    }));
  }

  handleChange(name, value) {
    this.setState((prevState, props) => ({
      ...prevState,
      inputs: {
        ...prevState.inputs,
        [name]: value
      }
    }));
  }

  handleSubmit() {
    const { onSubmitForm, scheme } = this.props;
    const { inputs } = this.state;
    const fData = new FormData();
    scheme.inputs.forEach((input, i) => {
      fData.append(input, inputs[input]);
    });
    onSubmitForm(fData);
  }

  render() {
    const { inputs } = this.state;
    const { scheme, identifier, classes } = this.props;
    return (
      <div className={classes.container}>
        {scheme.inputs.map((input, i) => {
          const InputItem = INPUT_TYPE[scheme.inputTypes[i]];
          return (
            <InputItem
              key={`${i}_${input}_input_${identifier}`}
              handleChange={this.handleChange}
              value={inputs[input]}
              inputName={input}
              label={scheme.inputLabels[i]}
              identifier={identifier}
            />
          );
        })}
        <Button
          size="small"
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={this.handleSubmit}
        >
          Enviar
        </Button>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Form);
