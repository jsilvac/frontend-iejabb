// Para cada input type hacer validaciones... aquí...
export const validateEmail = text => {
  const regexEmailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
  return {
    isValid: regexEmailPattern.test(text),
    error: 'El e-mail no es valido'
  };
};

export const validateRequired = text => {
  return {
    isValid: text === '' ? false : true,
    error: 'El campo es requerido'
  };
};

export const validateLength = (text, length) => {
  return {
    isValid: text.length > length ? false : true,
    error: `El numero de caracteres (${length}) se ha exedido`
  };
};
