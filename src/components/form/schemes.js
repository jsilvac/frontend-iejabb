export const newsScheme = {
  inputs: ['title', 'author', 'publishedDate', 'content', 'img'],
  inputLabels: [
    'Titulo',
    'Autor/es',
    'Fecha de publicación',
    'Contenido',
    'Subir Imagen'
  ],
  inputTypes: ['text', 'text', 'date', 'multiText', 'image']
};

export const eventScheme = {
  inputs: ['title', 'publishedDate', 'content', 'bannerAbstract', 'img'],
  inputLabels: [
    'Titulo',
    'Fecha de publicación',
    'Contenido',
    'Abstract del banner',
    'Subir Imagen'
  ],
  inputTypes: ['text', 'date', 'multiText', 'text', 'image']
};

export const userScheme = {};
