import React from 'react';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from '@material-ui/pickers';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { fade, withStyles } from '@material-ui/core/styles';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

const styles = theme => ({
  formControl: {
    display: 'flex',
    flexDirection: 'column',
    margin: '10px 5px'
  },
  imageInput: {
    width: '100%'
  },
  button: {
    margin: '10px'
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto'
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200
      }
    }
  }
});

const TextInput = props => {
  const { classes, handleChange, value, inputName, label, identifier } = props;
  return (
    <FormControl className={classes.formControl}>
      <InputLabel htmlFor={`${inputName}_${identifier}_input`}>
        {label}
      </InputLabel>
      <Input
        id={`${inputName}_${identifier}_input`}
        className={classes.input}
        value={value || ''}
        type="text"
        name={inputName}
        onChange={e => handleChange(inputName, e.target.value)}
        inputProps={{
          'aria-label': `${inputName}_${identifier}_input`
        }}
      />
    </FormControl>
  );
};

const MultiTextInput = props => {
  const { classes, handleChange, value, inputName, label, identifier } = props;
  return (
    <FormControl className={classes.formControl}>
      <InputLabel htmlFor={`${inputName}_${identifier}_multiline_input`}>
        {label}
      </InputLabel>
      <Input
        id={`${inputName}_${identifier}_multiline_input`}
        className={classes.input}
        value={value || ''}
        type="text"
        multiline
        rows="4"
        rowsMax="10"
        onChange={e => handleChange(inputName, e.target.value)}
        inputProps={{
          'aria-label': `${inputName}_${identifier}_multiline_input`
        }}
      />
    </FormControl>
  );
};

const DateInput = props => {
  const { handleChange, value, inputName, label, identifier } = props;
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        margin="normal"
        id={`${inputName}_${identifier}_date_input`}
        label={label}
        format="MM/dd/yyyy"
        value={value}
        onChange={date => handleChange(inputName, date)}
        KeyboardButtonProps={{
          'aria-label': { label }
        }}
      />
    </MuiPickersUtilsProvider>
  );
};

const ImageInput = props => {
  const { classes, handleChange, inputName, value, label, identifier } = props;
  return (
    <div className={classes.imageInput}>
      <input
        accept="image/*"
        style={{ display: 'none' }}
        id={`${inputName}_${identifier}_image_input`}
        multiple
        type="file"
        onChange={e => handleChange(inputName, e.target.files[0])}
      />
      <label htmlFor={`${inputName}_${identifier}_image_input`}>
        <Button
          variant="contained"
          color="default"
          component="span"
          className={classes.button}
          startIcon={<CloudUploadIcon />}
        >
          {label}
        </Button>
      </label>
      {value && value.size > 1 ? (
        <Chip
          avatar={<Avatar alt={value.name} src={URL.createObjectURL(value)} />}
          label={value.name}
          onDelete={() => handleChange(inputName, null)}
        />
      ) : null}
    </div>
  );
};

const INPUT_TYPE = {
  text: withStyles(styles, { withTheme: true })(TextInput),
  multiText: withStyles(styles, { withTheme: true })(MultiTextInput),
  date: withStyles(styles, { withTheme: true })(DateInput),
  image: withStyles(styles, { withTheme: true })(ImageInput)
};

export default INPUT_TYPE;
