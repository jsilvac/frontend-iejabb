import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import { useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import usePrevious from '../hooks/usePrevious';
import NewsItem from './newsItem';
import Wait from './common/Wait';
import Info from './common/Info';

const styles = theme => ({
  stepper: {
    width: '100%',
    flexGrow: 1
  },
  newsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    margin: '10px',
    flexWrap: 'wrap'
  }
});

function NewsContainer(props) {
  const { classes, newsList, loading, stepperOn, nItemsPStep } = props;
  const [activeStep, setActiveStep] = useState(0);
  const theme = useTheme();
  const prevNewsList = usePrevious(newsList);

  let nSteps = 0;
  let newsToShow = newsList;
  useEffect(() => {
    if (prevNewsList && prevNewsList.length !== newsList.length) {
      setActiveStep(0);
    }
  }, [prevNewsList, newsList]);

  if (loading) return <Wait text="Se están cargando las noticias" />;
  if (newsList.length === 0)
    return <Info text="No se han encontrado noticias" />;
  if (!stepperOn) {
    newsToShow = newsList.slice(0, nItemsPStep || 6);
    return (
      <div className={classes.newsContainer}>
        {newsToShow.map((news, i) => (
          <NewsItem key={news._id} data={news} />
        ))}
      </div>
    );
  } else if (stepperOn && nItemsPStep) {
    newsToShow = newsList.slice(
      nItemsPStep * activeStep,
      (activeStep + 1) * nItemsPStep
    );
    nSteps = !!(newsList.length % nItemsPStep)
      ? parseInt(newsList.length / nItemsPStep) + 1
      : parseInt(newsList.length / nItemsPStep);
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  return (
    <div>
      <div className={classes.newsContainer}>
        {newsToShow.map((news, i) => (
          <NewsItem key={news._id} data={news} />
        ))}
      </div>
      <MobileStepper
        variant="text"
        steps={nSteps}
        position="static"
        activeStep={activeStep}
        className={classes.stepper}
        nextButton={
          <Button
            size="small"
            onClick={handleNext}
            disabled={activeStep === nSteps - 1}
          >
            Siguiente
            {theme.direction === 'rtl' ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {theme.direction === 'rtl' ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
            Anterior
          </Button>
        }
      />
    </div>
  );
}

export default withStyles(styles)(NewsContainer);
