import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  }
});

class ListOfLinks extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.list}>
        <List>
          {['Inicio', 'Plataformas', 'Institución', 'Noticias', 'Nosotros'].map(
            (text, index) => (
              <ListItem button key={text}>
                <ListItemText primary={text} />
              </ListItem>
            )
          )}
        </List>
        <Divider />
        <List>
          {['Contactenos'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
}

ListOfLinks.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ListOfLinks);
