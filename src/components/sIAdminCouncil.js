import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import * as fromReducers from '../reducers/index';
import Wait from './common/Wait';
import MemberCard from './common/memberCard';
import { infoSectionLabel } from '../constants/strings';

const styles = theme => ({
  container: {
    width: '100%',
    boxSizing: 'border-box',
    display: 'flex',
    flexDirection: 'column'
  },
  title: {
    margin: '20px',
    textAlign: 'center'
  },
  description: {
    boxSizing: 'border-box',
    margin: '10px',
    whiteSpace: 'pre-line'
  },
  subtitle: {
    margin: '10px'
  },
  cardsContainer: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  }
});

function SIInstituGov(props) {
  const { adminCouncil, classes } = props;
  if (!adminCouncil) return <Wait text="Se está cargando la información" />;

  return (
    <div className={classes.container}>
      <Typography variant="h2" className={classes.title}>
        {infoSectionLabel.adminCouncil}
      </Typography>
      <Typography variant="body1" className={classes.description}>
        {adminCouncil.description}
      </Typography>
      <Typography variant="h6" className={classes.subtitle}>
        Lista de integrantes
      </Typography>
      <div className={classes.cardsContainer}>
        {adminCouncil.members.map((member, i) => (
          <MemberCard
            key={`${member.id}-${i}`}
            name={member.name}
            position={member.position}
            image={member.pictureUrl}
          />
        ))}
      </div>
    </div>
  );
}
const mapState = state => ({
  adminCouncil: fromReducers.getAdminCouncilInfo(state)
});

export default connect(mapState)(withStyles(styles)(SIInstituGov));
