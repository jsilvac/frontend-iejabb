import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import MobileStepper from '@material-ui/core/MobileStepper';
import { useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import usePrevious from '../hooks/usePrevious';
import * as fromReducers from '../reducers';
import EventItemSmall from './eventItemSmall';
import Wait from './common/Wait';
import Info from './common/Info';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: 'inline'
  },
  stepper: {
    width: '100%',
    flexGrow: 1
  }
}));

function EventContainerSmall(props) {
  const { eventList, loading, stepperOn, nItemsPStep } = props;
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const theme = useTheme();
  const prevEventList = usePrevious(eventList);

  let nSteps = 0;
  let eventsToShow = eventList;
  useEffect(() => {
    if (prevEventList && prevEventList.length !== eventList.length) {
      setActiveStep(0);
    }
  }, [prevEventList, eventList]);

  if (loading) return <Wait text="Se están cargando los eventos" />;
  if (eventList.length === 0) return <Info text="No hay eventos" />;

  if (!stepperOn) {
    eventsToShow = eventList.slice(0, nItemsPStep || 6);
    return (
      <List className={classes.root}>
        {eventsToShow.map((event, i) => (
          <React.Fragment key={`event-${i}`}>
            <EventItemSmall eventInfo={event} />
            <Divider variant="inset" component="li" />
          </React.Fragment>
        ))}
      </List>
    );
  } else if (stepperOn && nItemsPStep) {
    eventsToShow = eventList.slice(
      nItemsPStep * activeStep,
      (activeStep + 1) * nItemsPStep
    );
    nSteps = !!(eventList.length % nItemsPStep)
      ? parseInt(eventList.length / nItemsPStep) + 1
      : parseInt(eventList.length / nItemsPStep);
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  return (
    <div>
      <List className={classes.root}>
        {eventsToShow.map((event, i) => (
          <React.Fragment key={`event-${i}`}>
            <EventItemSmall eventInfo={event} />
            <Divider variant="inset" component="li" />
          </React.Fragment>
        ))}
      </List>
      <MobileStepper
        variant="text"
        steps={nSteps}
        position="static"
        activeStep={activeStep}
        className={classes.stepper}
        nextButton={
          <Button
            size="small"
            onClick={handleNext}
            disabled={activeStep === nSteps - 1}
          >
            Siguiente
            {theme.direction === 'rtl' ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {theme.direction === 'rtl' ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
            Anterior
          </Button>
        }
      />
    </div>
  );
}

const mapState = state => ({
  loading: state.event.fetch_isLoading,
  eventList: fromReducers.getEventList(state)
});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(EventContainerSmall);
