import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import MobileStepper from '@material-ui/core/MobileStepper';
import { useTheme } from '@material-ui/core/styles';

import usePrevious from '../hooks/usePrevious';
import * as fromReducers from '../reducers';
import NewsItemSmall from './newsItemSmall';
import Wait from './common/Wait';
import Info from './common/Info';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper
  },
  inline: {
    display: 'inline'
  },
  stepper: {
    width: '100%',
    flexGrow: 1
  }
}));

function NewsContainerSmall(props) {
  const { newsList, loading, stepperOn, nItemsPStep } = props;
  const [activeStep, setActiveStep] = React.useState(0);
  const classes = useStyles();
  const theme = useTheme();
  const prevNewsList = usePrevious(newsList);

  let nSteps = 0;
  let newsToShow = newsList;
  useEffect(() => {
    if (prevNewsList && prevNewsList.length !== newsList.length) {
      setActiveStep(0);
    }
  }, [prevNewsList, newsList]);

  if (loading) return <Wait text="Se están cargando las noticias" />;
  if (newsList.length === 0) return <Info text="No hay noticias" />;

  if (!stepperOn) {
    newsToShow = newsList.slice(0, nItemsPStep || 6);
    return (
      <List className={classes.root}>
        {newsToShow.map((news, i) => (
          <React.Fragment key={`news-${i}`}>
            <NewsItemSmall newsInfo={news} />
            <Divider variant="inset" component="li" />
          </React.Fragment>
        ))}
      </List>
    );
  } else if (stepperOn && nItemsPStep) {
    newsToShow = newsList.slice(
      nItemsPStep * activeStep,
      (activeStep + 1) * nItemsPStep
    );
    nSteps = !!(newsList.length % nItemsPStep)
      ? parseInt(newsList.length / nItemsPStep) + 1
      : parseInt(newsList.length / nItemsPStep);
  }

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  return (
    <div>
      <List className={classes.root}>
        {newsToShow.map((news, i) => (
          <React.Fragment key={`news-${i}`}>
            <NewsItemSmall newsInfo={news} />
            <Divider variant="inset" component="li" />
          </React.Fragment>
        ))}
      </List>
      <MobileStepper
        variant="text"
        steps={nSteps}
        position="static"
        activeStep={activeStep}
        className={classes.stepper}
        nextButton={
          <Button
            size="small"
            onClick={handleNext}
            disabled={activeStep === nSteps - 1}
          >
            Siguiente
            {theme.direction === 'rtl' ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {theme.direction === 'rtl' ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
            Anterior
          </Button>
        }
      />
    </div>
  );
}

const mapState = state => ({
  loading: state.news.fetch_isLoading,
  newsList: fromReducers.getNewsList(state)
});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(NewsContainerSmall);
