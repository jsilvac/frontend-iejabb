import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles, useTheme } from '@material-ui/core/styles';

import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Form from './form/index';
import { TabPanel, a11yProps } from './common/tabs';
import { createEvent, fetchEvent } from '../actions/event';
import EventContainerSmall from './eventContainerSmall';
import { eventScheme } from './form/schemes';
import Alert, { alertTypes } from './common/alert';
import CustomBackdrop from './common/customBackdrop';

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    maxWidth: '600px'
  }
});

function CRUDNews(props) {
  const {
    classes,
    onSubmitCreate,
    fetchEventList,
    createError,
    createSuccess,
    creating
  } = props;
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  useEffect(() => {
    fetchEventList();
  }, [fetchEventList]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = index => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Lista" {...a11yProps(0)} />
          <Tab label="Añadir" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <h3>Eventos</h3>
          <EventContainerSmall stepperOn nItemsPStep={6} />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <h3>Agregar evento</h3>
          <Form
            scheme={eventScheme}
            onSubmitForm={onSubmitCreate}
            identifier="create-event-form"
            reset={createSuccess}
            success={createSuccess}
            error={createError}
          />
        </TabPanel>
      </SwipeableViews>
      <CustomBackdrop open={creating} />
      <Alert
        open={!!(createSuccess || createError !== '')}
        type={createSuccess ? alertTypes.success : alertTypes.error}
        text={
          createSuccess
            ? 'Se ha creado el evento'
            : 'Hubo un error al crear el evento'
        }
      />
    </div>
  );
}

const mapState = state => ({
  creating: state.event.create_isLoading,
  createError: state.event.create_error,
  createSuccess: state.event.create_success
});

const mapDispatch = dispatch => ({
  onSubmitCreate: values => {
    dispatch(createEvent(values));
  },
  fetchEventList: () => dispatch(fetchEvent())
});

export default connect(mapState, mapDispatch)(withStyles(styles)(CRUDNews));
