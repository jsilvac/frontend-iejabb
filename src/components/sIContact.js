import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Grid } from '@material-ui/core';

import * as fromReducers from '../reducers/index';

import Wait from '../components/common/Wait';
import LeafMap from '../components/common/LeafMap';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: '10px',
    height: '100%',
    width: '100%',
    textAlign: 'center',
    justifyContent: 'center'
  },
  title: {
    alignSelf: 'center',
    margin: '30px'
  },
  info: {
    flex: 1
  },
  section: {
    margin: '20px 0px'
  }
});

function SIContact(props) {
  const { contactInfo, classes } = props;
  if (!contactInfo) return <Wait text="Se está cargando la información" />;
  const sections = [
    {
      label: 'Departamento',
      info: contactInfo.state
    },
    {
      label: 'Ciudad',
      info: contactInfo.city
    },
    {
      label: 'Telefono',
      info: contactInfo.phone
    },
    {
      label: 'Dirección',
      info: contactInfo.address
    }
  ];
  return (
    <div className={classes.container}>
      <LeafMap
        lat={contactInfo.latitude}
        lng={contactInfo.longitude}
        zoom={16}
        textPrimary={contactInfo.schoolName}
        textSecondary={contactInfo.address}
      />
      <div className={classes.info}>
        <Typography variant="h4" className={classes.title}>
          {contactInfo.schoolName}
        </Typography>
        {sections.map((section, i) => (
          <div className={classes.sectionContainer} key={section.label}>
            <Grid container alignItems="center">
              <Grid item xs>
                <Typography gutterBottom variant="h6">
                  {section.label}
                </Typography>
              </Grid>
              <Grid item xs>
                <Typography color="textSecondary" variant="body1">
                  {section.info}
                </Typography>
              </Grid>
            </Grid>
          </div>
        ))}
      </div>
    </div>
  );
}

const mapState = state => ({
  contactInfo: fromReducers.getContactInfo(state)
});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(withStyles(styles)(SIContact));
