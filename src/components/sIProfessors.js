import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Avatar from '@material-ui/core/Avatar';

import * as fromReducers from '../reducers/index';
import Wait from './common/Wait';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  cardsContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    width: '100%'
  },
  title: { margin: '30px', alignSelf: 'center' },
  paper: {
    padding: theme.spacing(1),
    alignItems: 'center',
    marginBottom: theme.spacing(1),
    display: 'flex',
    flex: 1,
    minWidth: '350px',
    height: 'max-content',
    margin: '10px'
  },
  cardImage: {
    width: 80,
    height: 80
  },
  cardContent: {
    width: 'auto',
    margin: '2px 10px'
  },
  cardTitle: {
    width: '260px',
    color: theme.palette.primary.main
  },
  cardDescription: {
    width: '260px'
  },
  divider: {
    margin: theme.spacing(2, 0)
  }
});

function SIProfessors(props) {
  const { professors, classes } = props;
  if (!professors) return <Wait text="Se está cargando la información" />;

  return (
    <div className={classes.container}>
      <Typography variant="h2" className={classes.title}>
        Nuestros Profesores
      </Typography>
      <div className={classes.cardsContainer}>
        {professors.map((professor, i) => (
          <Paper
            key={`professor-${professor.id}-${i}`}
            variant="outlined"
            square
            className={classes.paper}
            zeroMinWidth
          >
            <Avatar
              alt="Remy Sharp"
              src={professor.pictureUrl + `#${new Date()}`}
              className={classes.cardImage}
            />
            <div className={classes.cardContent}>
              <Typography variant="h6" className={classes.cardTitle}>
                {professor.name}
              </Typography>
              <Typography variant="body2" className={classes.cardDescription}>
                {professor.area}
              </Typography>
            </div>
          </Paper>
        ))}
      </div>
    </div>
  );
}
const mapState = state => ({
  professors: fromReducers.getProfessorsInfo(state)
});

export default connect(mapState)(
  withStyles(styles, { useTheme: true })(SIProfessors)
);
