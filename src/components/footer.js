import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

// Icons
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';

import * as fromReducers from '../reducers';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(1),
    margin: 0,
    boxSizing: 'border-box'
  },
  chip: {
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(1)
  },
  section1: {
    margin: theme.spacing(3, 2)
  },
  section2: {
    margin: theme.spacing(2)
  },
  title: {
    marginBottom: theme.spacing(1)
  },
  buttonsContainer: {
    display: 'flexbox',
    direction: 'row'
  }
}));

function Footer(props) {
  const { contactInfo } = props;
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.section1}>
        <Grid container className={classes.title} alignItems="center">
          <Grid item xs>
            <Typography gutterBottom variant="h6">
              Contactanos
            </Typography>
          </Grid>
        </Grid>
        <Typography color="textSecondary" variant="body2">
          {contactInfo
            ? `${contactInfo.address} Teléfono: ${contactInfo.phone}`
            : '-'}
        </Typography>
        <Typography color="textSecondary" variant="body1">
          {contactInfo ? `${contactInfo.city} - ${contactInfo.state}` : '-'}
        </Typography>
      </div>
      <Divider variant="middle" />
      <div className={classes.section2}>
        <Grid className={classes.title} container alignItems="center">
          <Grid item xs>
            <Typography gutterBottom variant="h6">
              Siguenos
            </Typography>
          </Grid>
        </Grid>
        <div>
          <a href={contactInfo ? contactInfo.facebook : '#'}>
            <Chip
              className={classes.chip}
              clickable
              icon={<FacebookIcon />}
              label="Facebook"
            />
          </a>
          <a href={contactInfo ? contactInfo.twitter : '#'}>
            <Chip
              className={classes.chip}
              clickable
              icon={<TwitterIcon />}
              label="Twitter"
            />
          </a>
          <a href={contactInfo ? contactInfo.instagram : '#'}>
            <Chip
              className={classes.chip}
              clickable
              icon={<InstagramIcon />}
              label="Instagram"
            />
          </a>
        </div>
      </div>
    </div>
  );
}

const mapState = state => ({
  contactInfo: fromReducers.getContactInfo(state)
});

const mapDispatch = dispatch => ({});

export default connect(mapState, mapDispatch)(Footer);
