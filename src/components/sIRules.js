import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import * as fromReducers from '../reducers/index';
import Wait from '../components/common/Wait';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'center',
    alignContent: 'baseline'
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    minWidth: '300px',
    margin: theme.spacing(1),
    padding: '20px',
    height: 'max-content',
    maxHeight: '900px',
    [theme.breakpoints.down(750)]: {
      width: '100%'
    }
  },
  title: {
    width: '100%',
    textAlign: 'center'
  },
  body: {
    width: '100%',
    whiteSpace: 'pre-line'
  }
});

function SIRules(props) {
  const { rules, classes } = props;
  if (!rules) return <Wait text="Se está cargando la información" />;

  return (
    <div className={classes.container}>
      <Paper square className={classes.paper}>
        <Typography variant="h6" className={classes.title}>
          Descripción
        </Typography>
        <Typography variant="body1" className={classes.body}>
          {rules.description}
        </Typography>
      </Paper>
      <Paper square className={classes.paper}>
        <Typography variant="h6" className={classes.title}>
          Reglamentación
        </Typography>
        <Typography variant="body1" className={classes.body}>
          {rules.content}
        </Typography>
      </Paper>
    </div>
  );
}
const mapState = state => ({
  rules: fromReducers.getRules(state)
});

export default connect(mapState)(withStyles(styles)(SIRules));
