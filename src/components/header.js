import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Logo from '../assets/images/logo.png';
import { Link } from 'react-router-dom';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import CommentIcon from '@material-ui/icons/Comment';
import EventIcon from '@material-ui/icons/Event';
import ReportProblemIcon from '@material-ui/icons/ReportProblem';
import DvrIcon from '@material-ui/icons/Dvr';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import { useHistory } from 'react-router-dom';

import * as fromReducers from '../reducers';
import * as authActions from '../actions/auth';
import HeaderButton from './headerButton';

const MENU_ITEM_HEIGHT = 48;

const styles = theme => ({
  root: {
    width: '100%'
  },
  grow: {
    flexGrow: 1
  },
  buttonsContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  logoContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'block',
      overflow: 'visible'
    }
  },
  avatar: {
    margin: 10
  },
  bigAvatar: {
    margin: '5px 10px 5px 5px',
    width: 40,
    height: 40
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.primary.text
  },
  accountIcon: {
    color: theme.palette.secondary.main
  }
});

const Header = props => {
  const { classes, platformUrl, authenticated, logOut } = props;

  const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickAdmin = () => {
    setAnchorEl(null);
    history.push('/admin');
  };

  const handleClickLogOut = () => {
    setAnchorEl(null);
    logOut();
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Link to={'/'} className={classes.link}>
            <div className={classes.logoContainer}>
              <Avatar alt="IE" src={Logo} className={classes.bigAvatar} />
              <Typography
                className={classes.title}
                variant="h6"
                color="inherit"
                noWrap
              >
                I.E.T Jose Agustín Blanco Barros
              </Typography>
            </div>
          </Link>
          <div className={classes.grow} />
          <div className={classes.buttonsContainer}>
            <Link to={'/school-info'} className={classes.link}>
              <HeaderButton text="Institución" Icon={AccountBalanceIcon} />
            </Link>
            <Link to={'/news'} className={classes.link}>
              <HeaderButton text="Noticias" Icon={CommentIcon} />
            </Link>
            <Link to={'/event'} className={classes.link}>
              <HeaderButton text="Eventos" Icon={EventIcon} />
            </Link>
            <Link to={'/pqrForm'} className={classes.link}>
              <HeaderButton text="Contactenos" Icon={ReportProblemIcon} />
            </Link>
            <a href={platformUrl} className={classes.link}>
              <HeaderButton text="Plataforma" Icon={DvrIcon} />
            </a>
            {authenticated ? (
              <div>
                <IconButton
                  aria-label="more"
                  aria-controls="account-menu"
                  aria-haspopup="true"
                  className={classes.accountIcon}
                  onClick={handleClick}
                >
                  <AccountCircleIcon />
                </IconButton>
                <Menu
                  id="account-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={open}
                  onClose={handleClose}
                  PaperProps={{
                    style: {
                      maxHeight: MENU_ITEM_HEIGHT * 4.5,
                      width: 200
                    }
                  }}
                >
                  <MenuItem onClick={handleClickAdmin}>Administración</MenuItem>
                  <MenuItem onClick={handleClickLogOut}>Cerrar sesión</MenuItem>
                </Menu>
              </div>
            ) : (
              <Link to={'/login'} className={classes.link}>
                <HeaderButton text="Ingresar" Icon={VpnKeyIcon} />
              </Link>
            )}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
};

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapState = state => ({
  platformUrl: fromReducers.getSchoolPlatformUrl(state),
  authenticated: state.auth.authenticated
});

const mapDispatch = dispatch => ({
  logOut: () => dispatch(authActions.logOut())
});

export default connect(mapState, mapDispatch)(withStyles(styles)(Header));
