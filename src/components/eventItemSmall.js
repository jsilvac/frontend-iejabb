import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';

import * as eventActions from '../actions/event';
import AlertButton from './common/alertButton';
import FormButton from './common/formButton';
import USER_IMAGE from '../assets/images/user.png';
import { eventScheme } from './form/schemes';
import { truncateString, TINY_DESC_MAX_CHARACTERS } from '../utils/strings';

const useStyles = makeStyles(theme => ({
  inline: {
    display: 'inline'
  }
}));

function EventItemSmall(props) {
  const classes = useStyles();
  const {
    eventInfo,
    deleteEvent,
    editEvent,
    editError,
    editSuccess,
    editing,
    deleting
  } = props;

  const handleDelete = () => {
    deleteEvent(eventInfo._id);
  };

  const handleEdit = newContent => {
    editEvent(eventInfo._id, newContent);
  };

  return (
    <ListItem alignItems="center">
      <ListItemAvatar>
        <Avatar
          alt="Cindy Baker"
          src={eventInfo.img ? `${eventInfo.img}?${Date.now()}` : USER_IMAGE}
        />
      </ListItemAvatar>
      <ListItemText
        primary={truncateString(eventInfo.title, TINY_DESC_MAX_CHARACTERS)}
        secondary={
          <React.Fragment>
            <Typography
              component="span"
              variant="body2"
              className={classes.inline}
              color="textPrimary"
            >
              {eventInfo.author}
            </Typography>
            {`${truncateString(eventInfo.content, TINY_DESC_MAX_CHARACTERS)}`}
          </React.Fragment>
        }
      />
      <FormButton
        onSubmit={handleEdit}
        scheme={eventScheme}
        initialContent={eventInfo}
        title="Editar evento"
        identifier="edit-event-form"
        reset={editSuccess}
        formError={editError}
        formSuccess={editSuccess}
        formLoading={editing || deleting}
        textSuccess="Se ha editado la evento"
        textError="Hubo un error al editar la evento"
      >
        <EditIcon />
      </FormButton>
      <AlertButton
        onAgree={handleDelete}
        title="Eliminar evento"
        content="¿Desea eliminar esta evento?"
      >
        <DeleteForeverIcon />
      </AlertButton>
    </ListItem>
  );
}

const mapState = state => ({
  deleting: state.event.delete_isLoading,
  editing: state.event.edit_isLoading,
  editError: state.event.edit_error,
  editSuccess: state.event.edit_success
});

const mapDispatch = dispatch => ({
  deleteEvent: id => dispatch(eventActions.deleteEvent(id)),
  editEvent: (id, content) => dispatch(eventActions.editEvent(id, content)),
  resetSuccess: () => dispatch(eventActions.resetEventSuccess()),
  resetError: () => dispatch(eventActions.resetEventError())
});

export default connect(mapState, mapDispatch)(EventItemSmall);
