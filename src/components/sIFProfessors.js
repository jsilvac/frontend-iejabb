import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import * as schoolInfoActions from '../actions/schoolInfo';
import * as fromReducers from '../reducers';
import Alert, { alertTypes } from './common/alert';
import Info from './common/Info';
import Wait from './common/Wait';
import MemberCreator from './common/memberCreator';
import MemberCard from './common/memberCard';
import MEMBER_IMAGE from '../assets/images/default_member.jpg';
import CustomBackdrop from '../components/common/customBackdrop';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center'
  },
  title: {
    margin: '40px 10px'
  },
  content: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    padding: '20px',
    boxSizing: 'border-box'
  },
  membersContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  textInput: {
    margin: theme.spacing(1)
  },
  button: {
    width: 'max-content',
    alignSelf: 'center',
    margin: '20px 10px 0px 10px'
  }
});

const memberFields = [
  { name: 'name', label: 'Nombre' },
  { name: 'area', label: 'Área' }
];

function SIFContact(props) {
  const {
    classes,
    professors,
    isLoading,
    editProfessors,
    editSuccess,
    editError,
    editing
  } = props;
  const [membersList, setMembersList] = useState([]);
  const [createdMembers, setCreatedMembers] = useState([]);
  const [deletedMembers, setDeletedMembers] = useState([]);
  const [listOfImages, setListOfImages] = useState([]);

  useEffect(() => {
    if (professors) {
      setMembersList(professors);
    }
  }, [professors]);

  const handleSubmitClick = () => {
    const data = {
      createdMembers,
      deletedMembers
    };
    const fData = new FormData();
    fData.append('data', JSON.stringify(data));
    listOfImages.forEach((image, i) => {
      if (image) fData.append('images', image, i);
    });
    setCreatedMembers([]);
    setDeletedMembers([]);
    setListOfImages([]);
    editProfessors(fData);
  };

  const handleCreateMember = data => {
    setCreatedMembers([...createdMembers, data.member]);
    if (data.image) setListOfImages([...listOfImages, data.image]);
  };

  const handleDeleteOldMember = id => {
    setDeletedMembers([...deletedMembers, membersList[id].id]);
    setMembersList(membersList.filter((m, i) => i !== id));
  };

  const handleDeleteNewMember = id => {
    setCreatedMembers(createdMembers.filter((m, i) => i !== id));
    setListOfImages(listOfImages.filter((m, i) => i !== id));
  };

  if (!professors && isLoading)
    return <Wait text="Se está cargando los Profesores" />;

  if (!professors) return <Info text="No se ha cargado los Profesores" />;

  return (
    <div className={classes.container}>
      <Typography variant="h4" component="h6" className={classes.title}>
        Editar Profesores
      </Typography>
      <div className={classes.content}>
        <MemberCreator
          onSubmit={handleCreateMember}
          memberFields={memberFields}
          memberType="professors"
          memberLabel="Profesor"
        />
        <div className={classes.membersContainer}>
          {membersList.map((member, i) => (
            <MemberCard
              key={`${member.name}-${i}`}
              id={i}
              name={member.name}
              position={member.area}
              image={member.pictureUrl}
              onDelete={handleDeleteOldMember}
            />
          ))}
          {createdMembers.map((member, i) => (
            <MemberCard
              key={`${member.name}-${i}`}
              id={i}
              name={member.name}
              position={member.area}
              image={
                listOfImages[i]
                  ? URL.createObjectURL(listOfImages[i])
                  : MEMBER_IMAGE
              }
              onDelete={handleDeleteNewMember}
            />
          ))}
        </div>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmitClick}
          className={classes.button}
          disabled={editing}
        >
          Actualizar Información
        </Button>
        <Alert
          open={!!(editSuccess || editError !== '')}
          type={editSuccess ? alertTypes.success : alertTypes.error}
          text={
            editSuccess
              ? 'Se ha actualizado la infomación'
              : 'Error al actualizar la información'
          }
        />
        <CustomBackdrop open={editing} />
      </div>
    </div>
  );
}

const mapState = state => ({
  professors: fromReducers.getProfessorsInfo(state),
  isLoading: state.schoolInfo.isLoading,
  editSuccess: state.schoolInfo.professors_success,
  editError: state.schoolInfo.professors_error,
  editing: state.schoolInfo.professors_isLoading
});

const mapDispatch = dispatch => ({
  editProfessors: values => dispatch(schoolInfoActions.editSIProfessors(values))
});

export default withStyles(styles, { withTheme: true })(
  connect(mapState, mapDispatch)(SIFContact)
);
