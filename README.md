# App web
This Folder contains an App web for the for the <<I.E.T Jose Agustin Blanco Barros>>.The App developed with React, using Redux as state management tool. Redux-Saga is used to manage Redux asynchronous actions. Material-UI is used as Style framework.

## Setup the App
Install App dependencies, using the following command in the frontend folder:
> npm install
## Start app
To Start the app run in the frontend folder:
> npm start